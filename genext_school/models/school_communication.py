# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime



''' 
    An article can be published only by the administration
    it can be viewed by user who belongs to the specified groups
'''
class Article(models.Model):
    _name = 'school.article'


    @api.model
    def _get_school_groups(self):
        return [('category_id', '=', self.env.ref('genext_school.module_category_school').id )]

    @api.multi
    @api.depends('comment_ids')
    def _compute_comment(self):
        for rec in self:
            if rec.comment_ids:
                rec.comment_count = len(rec.comment_ids)

    @api.multi
    @api.depends('like_ids')
    def _compute_like(self):
        for rec in self:
            if rec.like_ids:
                rec.like_count = len(rec.like_ids)


    name            = fields.Char('Article Title', required=True)
    user_id         = fields.Many2one('res.users', default=lambda self: self.env.user)
    state           = fields.Selection([
                                ('unpublished','Unpublished'),
                                ('published','Published'),
                                ('canceled','Canceled')], 
                                default='unpublished')
    subtitle        = fields.Char('Subtitle')
    content         = fields.Text('Content', required=True)
    datetime    = fields.Datetime(default=fields.Datetime.now())
    group_ids       = fields.Many2many('res.groups', string='Groups', domain=_get_school_groups)
    image_ids       = fields.One2many('school.article.image', 'article_id', string="Images")
    # will be updated only from the external API
    views_nbr       = fields.Integer(string='Views Number')

    like_ids        = fields.One2many('school.article.like', 'article_id', string="Likes")
    like_count      = fields.Integer(string='Likes', compute=_compute_like)
    
    comment_ids     = fields.One2many('school.article.comment', 'article_id', string="Comments")
    comment_count   = fields.Integer(compute=_compute_comment)

    
    @api.multi
    def increment_view(self):
        for rec in self:
            rec.write({'views_nbr':rec.views_nbr+1})

    @api.one
    def set_to_unpublished(self):
        self.write({'state':'unpublished'})

    @api.one
    def set_to_published(self):
        self.write({'state':'published'})

    @api.one
    def set_to_canceled(self):
        self.write({'state':'canceled'})    


    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'canceled':
                raise UserError("You cannot delete this article, you need to cancel it first")
        return super(Article, self).unlink()

    

    @api.multi
    def write(self, vals):
        for rec in self:
            for img in rec.image_ids:
                url = 'http://localhost:8069/web/image?model='+img._name+'&id='+str(img.id)+'&field=image'
                print "## url ",url
        return super(Article, self).write(vals)



class ArticleImage(models.Model):
    _name  = 'school.article.image'
    _description = 'Image Related to an article'

    name    = fields.Char('Image name')
    description = fields.Char('Description of image')
    image = fields.Binary("Image", attachment=True)
    image_url  = fields.Char('Image url')
    article_id      = fields.Many2one('school.article', ondelete='cascade')

    @api.model
    def create(self,vals):
        res = super(ArticleImage, self).create(vals)
        if vals.get('image',False):
            domain = [
                ('res_model', '=', res._name),
                ('res_field', '=', 'image'),
                ('res_id', '=', res.id),
            ]
            attachment = self.env['ir.attachment'].search(domain)
            attachment.write({'public':True})
            print "ATTACHMENT ", attachment
            print "ATTACHMENT ID ", attachment.id
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            res['image_url'] = str(base_url)+'/web/content/'+str(attachment.id)
        return res

    # updated without test: check if the new image still got the same ir.attachment id and is public or not 
    @api.multi
    def write(self, vals):
        res = super(ArticleImage, self).write(vals)
        for rec in self:
            print "CONTENT OF VALS ",vals
            if vals.get('image',False):
                domain = [
                    ('res_model', '=', rec._name),
                    ('res_field', '=', 'image'),
                    ('res_id', '=', rec.id),
                ]
                attachment = self.env['ir.attachment'].search(domain)
                attachment.write({'public':True})
                print "ATTACHMENT ", attachment
                print "ATTACHMENT ID ", attachment.id
                print "ATTACHMENT PUBLIC ", attachment.public
        return res


class Comment(models.Model):
    _name  = 'school.article.comment'
    _description = 'Comments Related to an article'

    name        = fields.Text('Comment')
    user_id     = fields.Many2one('res.users', default=lambda self: self.env.user)
    datetime    = fields.Datetime(default=fields.Datetime.now())
    article_id  = fields.Many2one('school.article', ondelete='cascade')

    

class Like(models.Model):
    _name  = 'school.article.like'
    _description = 'Likes Related to an article'

    name        = fields.Char('Like')
    user_id     = fields.Many2one('res.users', default=lambda self: self.env.user)
    article_id  = fields.Many2one('school.article', ondelete='cascade')

    _sql_constraints = [('code_uniq', 'unique(user_id,article_id)','You already liked this article')]
    