#-*- encoding: utf-8 -*-
from . import school
from . import school_config_settings
from . import timetable
from . import attendance
from . import wizard
from . import sanction
from . import notification
from . import hr_employee
from . import exemption
from . import ir_report
# from . import school_communication