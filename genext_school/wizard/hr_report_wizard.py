# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)


class EmployeeInfoWizard(models.TransientModel):
    _name = 'employee.info.wizard'

    employee_id             = fields.Many2many('hr.employee',string="Employés", required=True)
    education               = fields.Many2many(relation="rh_fields_education", comodel_name='ir.model.fields',domain="[('model_name','=','hr.education')]", string='Études et diplômes')
    experience              = fields.Many2many(relation="rh_fields_experience",comodel_name='ir.model.fields',domain="[('model_name','=','hr.experience')]", string='Expériences professionnelles')
    job_information         = fields.Many2many(relation='rh_fields_job', comodel_name='ir.model.fields', domain="[('model_name','=','hr.job.line')]", string='Données sur le poste')    
    personal_information    = fields.Many2many(relation='rh_fields_employee', comodel_name='ir.model.fields', domain="[('model_name','=','hr.employee')]", string='État civil')

    def _build_contexts(self, data):
        result = {}
        result['employee_id'] = 'employee_id' in data['form'] and data['form']['employee_id'] or False
        result['personal_information'] = 'personal_information' in data['form'] and data['form']['personal_information'] or False
        result['education'] = 'education' in data['form'] and data['form']['education'] or False
        result['experience'] = 'experience' in data['form'] and data['form']['experience'] or False
        result['job_information'] = 'job_information' in data['form'] and data['form']['job_information'] or False
        return result

    def _print_report(self, data):
        return self.env['report'].get_action(self, 'genext_school.report_employeeinfo', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['employee_id','personal_information','education','experience','job_information'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)


class EmployeeInfoReport(models.AbstractModel):
    _name = 'report.genext_school.report_employeeinfo'

    def _get_civil_status_val(self,data,employee_id):
        used_context = data['form'].get('used_context', {})
        list_val = {}
        if used_context['personal_information']:
            # print "###### after if ",used_context['personal_information']
            employee = self.env['hr.employee'].search([('id','=',employee_id.id)])
            print "########### EMPLOYEE NAME",employee.name
            for line in used_context['personal_information']:
                obj = self.env['ir.model.fields'].browse(line)
                print "########### PERSONAL INFORMATION NAME", obj.name
                print "##########", employee_id[obj.name]
                key = str(obj.name)
                value = str(employee_id[obj.name])
                list_val[key] = value
        print "########## list_val",list_val
        return list_val

    def _get_job_val(self,data,employee_id):
        used_context = data['form'].get('used_context', {})
        list_val = {}
        if used_context['job_information']:
            print "###### after if ",used_context['job_information']
            employee = self.env['hr.employee'].search([('id','=',employee_id.id)])
            for line in used_context['job_information']:
                obj = self.env['ir.model.fields'].browse(line)
                print "########### JOB INFORMATION NAME", obj.name
                # print "##########", employee_id[obj.name]
                key = str(obj.name)
                print "######## KEY",key
                # value = str(employee_id[obj.name])
                # print "######## VALUE",value
                list_val[key] = ''
        print "########## list_val",list_val
        return list_val


    def _get_education_val(self,data,employee_id):
        used_context = data['form'].get('used_context', {})
        list_val = {}
        if used_context['education']:
            print "###### after if ",used_context['education']
            employee = self.env['hr.employee'].search([('id','=',employee_id.id)])
            for line in used_context['education']:
                obj = self.env['ir.model.fields'].browse(line)
                print "########### EDUCATION NAME", obj.name
                # print "##########", employee_id[obj.name]
                key = str(obj.name)
                print "######## KEY",key
                # value = str(employee_id[obj.name])
                # print "######## VALUE",value
                list_val[key] = ''
        print "########## list_val",list_val
        return list_val


    def _get_experience_val(self,data,employee_id):
        used_context = data['form'].get('used_context', {})
        list_val = {}
        if used_context['experience']:
            print "###### after if ",used_context['experience']
            employee = self.env['hr.employee'].search([('id','=',employee_id.id)])
            for line in used_context['experience']:
                obj = self.env['ir.model.fields'].browse(line)
                print "########### EXPERIENCE NAME", obj.name
                # print "##########", employee_id[obj.name]
                key = str(obj.name)
                print "######## KEY",key
                # value = str(employee_id[obj.name])
                # print "######## VALUE",value
                list_val[key] = ''
        print "########## list_val",list_val
        return list_val


    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        no_employee = True
        employee_id = None
        if used_context['employee_id'] and len(used_context['employee_id']):
            no_employee = False
            employee_id = self.env['hr.employee'].browse(used_context['employee_id'])

        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['hr.employee'],
            'data': data,
            'docs': employee_id,
            'no_employee': no_employee,
            'time': time,
            'civil_status': self._get_civil_status_val,
            'job_information': self._get_job_val,
            'education': self._get_education_val,
            'experience': self._get_experience_val,
        }
        return self.env['report'].render('genext_school.report_employeeinfo', docargs)
