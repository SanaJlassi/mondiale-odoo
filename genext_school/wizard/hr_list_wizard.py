# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)


class HrListWizard(models.TransientModel):
    _name = 'hr.list.wizard'

    @api.model
    def get_employees(self):
        return self.env['hr.employee'].search([])

    employee_id             = fields.Many2many('hr.employee',string="Employés", required=True, default=get_employees)
    employee_info           = fields.Many2many(relation='rh_list_fields_employee', comodel_name='ir.model.fields', domain="[('model_name','=','hr.employee')]", string='Formulaire Employés')
    
    def _build_contexts(self, data):
        result = {}
        result['employee_id']           = 'employee_id' in data['form'] and data['form']['employee_id'] or False
        result['employee_info']         = 'employee_info' in data['form'] and data['form']['employee_info'] or False
        return result

    def _print_report(self, data):
        return self.env['report'].get_action(self, 'genext_school.report_hrlist', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['employee_id', 'employee_info'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)


class HrListReport(models.AbstractModel):
    _name = 'report.genext_school.report_hrlist'

    def _get_employee_val(self,data):
        used_context = data['form'].get('used_context', {})
        list_val = {}
        if used_context['employee_info']:
            print "###### after if ",used_context['employee_info']
            for line in used_context['employee_info']:
                obj = self.env['ir.model.fields'].browse(line)
                print "########### EMPLOYEE INFORMATION NAME", obj.name
                key = str(obj.name)
                print "######## KEY",key
                list_val[key] = ''
        print "########## list_val",list_val
        return list_val


    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        employee_id = None
        if used_context['employee_id'] and len(used_context['employee_id']):
            employee_id = self.env['hr.employee'].browse(used_context['employee_id'])

        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['hr.employee'],
            'data': data,
            'docs': employee_id,
            'time': time,
            'employee_val': self._get_employee_val,
        }
        return self.env['report'].render('genext_school.report_hrlist', docargs)
