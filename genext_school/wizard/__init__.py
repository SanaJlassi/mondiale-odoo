# -*- coding: utf-8 -*-
from . import hr_report_wizard
from . import student_list_wizard
from . import student_sheet_wizard
from . import student_certificate_wizard
from . import trambinoscope_wizard
from . import conduct_daily_report
from . import hr_list_wizard
from . import student_list_wizard_excel