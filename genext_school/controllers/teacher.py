# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class SchoolTeacher(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }

    @http.route('/api/teacher/class/<int:class_id>/subjects', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_subject_by_class(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

        teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to find data related to logged teacher'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

        class_id = request.env['school.class'].search([('id','=',class_id)]) 

        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

        subject_list = []
        for subject in class_id.grade_id.subject_ids:
            if subject.id in teacher_id.subject_ids.ids:
                print "#### FOUND SUBJECT EQUAL ",subject.name
                subject_list.append({
                    'id':subject.id,
                    'name':subject.name,
                    'code':subject.code,
                })
            else:
                print "#### NOT TEACHERS SUBJECTd ",subject.name

        response['success'] = True
        response['data'] = subject_list
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)


    """
                            NOT TESTED 
    """
    @http.route('/api/teacher/contact', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_cantact(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
                       
        teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
        
        classes, parents = teacher._get_class_and_parents(teacher.id)
        print "#### parent ids from controller ",
        response['success'] = True
        response['data'] = ['parent','parent2']
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)



    @http.route('/api/teacher/task', type='http', auth="user", methods=['POST'],  csrf=False)
    def create_task(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
                       
        teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
        _logger.warning('### DATAAAAAAA :  (%s).', request.params.get('task',False))
        json_object = None
        if not request.params.get('task',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'task object not submited'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

        if request.params.get('all_student',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'all_student is missing'}
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

        # print "### DATA RECIEVED : ",request.params.get('task',False)
        #_logger.warning('### DATA RECIEVED :  (%s).', request.params.get('task',False))
        try:
            json_object = json.loads(request.params.get('task',False))
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize task object'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

        #_logger.warning('### JSON DATA  :  (%s).', json_object)
        #print "### DATA ",json_object

        date = datetime.strptime(json_object['deadline'], DEFAULT_SERVER_DATE_FORMAT).date()
        print "Date ",date
        _logger.warning('###  DATE  :  (%s).',date)
        
        if self._validate_object(json_object, response)['valide'] == False:
            response.pop('valide', None)
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:
            response.pop('valide', None)

        list_student = []
        if json_object['all_student'] == True:
            class_id = request.env['school.class'].search([('id','=',json_object['class_id'])])
            list_student = [(6,False,[y for y in class_id.student_ids.ids])]
            _logger.warning('######### ALL STUDENTS %s',list_student)
        else:
            list_student = [(6,False,[y for y in json_object['student_ids']])]
            _logger.warning('######### LIST OF STUDENTS %s',list_student)


        json_object = {
            'grade_id':json_object['grade_id'],
            'subject_id':json_object['subject_id'],
            'teacher_id':teacher.id,
            'name':json_object['name'],
            'academic_year_id':request.env['academic.year'].search([('active_year','=',True)]).id,
            'description':json_object['description'],
            'dead_line':date,
            'class_id':json_object['class_id'],
            'timing_system_periode_id':json_object['timing_system_periode_id'],
            'all_student':json_object['all_student'],
            'student_ids':list_student,
            'file_ids':self._prepar_file_objects(json_object['file_ids']),
            'type_task':'task',
        }
        context = request.env.context.copy()
        context['source'] = 'controller'
        request.env.context = context
        res = request.env['school.task'].create(json_object)
        if res:
            response['success'] = True
            response['data'] = False
        else: 
            response['success'] = False
            response['data'] = False
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

    def _prepar_file_objects(self, files):
        file_list = []
        for file in files:
            file['file'] = file['file'].replace(' ','+')
            base64 = file['file'].split("base64,")
            if len(base64):
                file_list.append((0,False,{'name':file['name'],'file_name':file['name'], 'description':file['description'],'content_type':base64[0],'file':base64[1]}))
        if len(file_list):
            return file_list
        else:
            return False

        #  {
        #     "timing_system_periode_id": 1, 
        #     "name": "test", 
        #     "class_id": 2, 
        #     "file_ids": [], 
        #     "student_ids": [246], 
        #     "deadline": "2017-09-22", 
        #     "all_student": true, 
        #     "description": "desc"
        #     'file_ids'[{'name':'file name', 'description':'description', 'file':'base64'}, {'name':'file name', 'description':'description', 'file':'base64'}],
        # }

        #"file_ids":[{"name":"file name", "description":"description", "file":"iVBORw0KGgoAAAANSUhEUgAAANIAAAAzCAYAAADigVZlAAAQN0lEQVR4nO2dCXQTxxnHl0LT5jVteHlN+5q+JCKBJITLmHIfKzBHHCCYBAiEw+I2GIMhDQ0kqQolIRc1SV5e+prmqX3JawgQDL64bK8x2Ajb2Bg7NuBjjSXftmRZhyXZ1nZG1eL1eGa1kg2iyua9X2TvzvHNN/Ofb2Z2ZSiO4ygZGZm+EXADZGSCgYAbICMTDATcABmZYCDgBsjIBAMBN0BGJhgIuAEyMsGA1wQdHZ1UV1cX5XK5qM7OzgcMRuNTrSbTEraq6strhdfzruTk5Wpz8q5c1l7Jyb6szc3K1l7RggtFxcWX2dvVB02mtmVOp3NIV2fnQFie2WyB5QS84TIy/YnXBFBI8BMM/pDqat0XzIVM08lTSVxyytn6jAuZV4FuzmtzclJz8/LT8vML0nJzr54HYkpLS88oTkxMMZ48mchlXrxUX1ffcBCUM8xms8lCkgk6pCT6aZvZvCrzYpbu2PfxHAg8l+obGmOt1vaJQBAPkvI5nM5fWyyWWTU1tfuA+IqOHDvGgehVCK4pA91oGZn+xluCAc0thtj4hCT72XOp9S0thi2FBQWPvb13z9RN61QH5s8NYxbMDct7KXyudt7MGeeWLFrwn8iVKz7auDZy3Z7dbzz91p43B8ZsjYLlDKmprd3/ffwpLjWNqbW32xcFuuEyMv2J2M1BJpMpKiExxZKZeamira1tvvqdt8OWL1l8asq4kNbRzz7NTRo7uuMPo4Y7Rz/zFBc64lluzHNDuZFDFe5PICx25/aY2B3bogf/dd9fKCA+CuytohOSkjuyLmtLXRwXGujGy8j0F8Qbdrt9bDpzQQ8jSHl5+dLt0VsOThgzwj7i6Se5kOHDuIljR9mXRrykjZj/wlVeSONHP8+FhykrJoeOsY8aNoQLAYJa9erShIPvvRsKhQTK/YleX3Pw5KlErpKt+iLQjZeR6S9IN35VXl75r3gw4HU6/Z6ojes/gMKAUQiKBQKiUvvLC1/MXL18WcKsaZOrJ4WObly7euUJsOQ7FjZ9Sh2IVC4oLhihZk6d1LB5/dpt+9R/hnuq4Xl5VwvT0jLKXS7XOHgaCAm0I2Rk+gL2os1mewXsiUw5uXlZn8T9LVI5ZWI1jEQTxozkgECgkDrmKqfrFy8ILwJ7om+3bNoQumTRwtDoqE0fTBsf2ggwg+jVBdOCT7eYwGfnti2bQXA6ME2nr9mbnHLOWV/fEI3WTdO0jMzdZjBAKWBwX8ojCqm8vOJoYvLp9qPfHTmy5rXlJ+BSbtzI5+5EI4ALRCTHHHpaQ8zWqOidO2IooBAKRKRDQDwGevJ4w8SQUR0e0bmB0QxEKh2IYsdbTW0zmIxM4/Wi4q9BfQMkCikCoAEUADgEeI3xOOVedkicp14e1V2uLwSpTwxNAPwRaGC7OQFqQp9xGDT+1ksUUubFrMoLFy/VL5g7+4ep48fa+P0Pz9jnn4H7JCcQBbP79V1rgJDmASE9um7NqvmxMdFbVateiwd7KKswHx+dwBKwzGq1jgDRrjQ7W5sB6hvsRUhQQCyh8Sg4xwW64/oTpUQ/CIm7xz652yg9flb40R+xIn5i/LWJKKSk5NOuwqIi7cSQkXooAD6ywE8YneDyLWrDuq/WR67+BvxcB5dtG9dGHgF7oZsgSuWFz555c0LISKcwIvHlAHSdnR0P37h5699pzIW6NrNlptFoIglJ7cOAgcTf40711nH3g5AguEH3/4YGaZPSj/6Ix/hGmKd/hXQqIanz5q1b8WA5VwOXdLwgoIjAsk2/Y1v0odUrXj0OT+vgNSCkjgXzZleANF3wpI6PRALxcDDt7BlTby+NWPgdqOPBisrKz8E+zFFXX79Sp9fjhKQiDAqjx6kRHmfCdHDWZek+zCp+gnac6i7XhxOSUkAExiZI7D32y73wtbKfy/CnPDdEISUkJjsrKiqPhocp86ZPGGeDSzkIWJa1Rq5ccXyDas1X8PBBuG9Cow8UE/yEaYYPeZybPnFcM1gGRh/6+KNhNbV1o7Mua29dysrOdblcQ4SvDHmMg5s/I2ZAxNP+bQz5zaVaABz0ij7kh6D7NVJnwL1NLJLXn47DCQmXjkXSqAnpFB4/CO2KkODjEE861B9i7VcKwPldgaQJQfKi4yFWkNZbPXzZuP4iQRobaLrBIhEpubP0xq2E9989MHnLpg3rX5hFlz3/1BMcWLaVRm/eeIieNL4KRhi450EjDxQOvAf2T+mrli9bDZaAq3Zu37b3nbf2zvnwg/d/DoRENbcYRmhzcn84n5peDkQ0FbNHUmMGjD/LtsGesnCi5GEEnYbLH+clP9ox6ABiRdKzmDz9ISR0wKgx7WJE7ILtxUUxlQQfGDFtQutC7cH1OUPIi8NbPWjZUtBgbIzApFMQhZSccrbrav61zAqWfWR79JbJ8+eG5Q97/HccfB0I/P4eEJADRigoJP6NBvgzBC715s2coTuwf9+0qI3rKbB3ooCQKCAkCgiJgkKCS7uWFuMbiUkpjpzcvCvg9yGIkFicwZiGeRMR7oQPB+x8VEy+5OcRDiDcoCdBErI/QsINdmH5pGiPAxUT6cQLxYjkY5D7aozdaiQNQ8iLoz+EhPY1i7FRg7ORKKTUtHSdVptTarPZhr737oFHgRj+7lmeVcRsjfrwxdkzc+DSDj50VU6Z0LR5/drDK5a8HLt4QfhusAfaBUQz8tDHHw/atE5FEhLkods6/ZfHjsdzZWXlJwRCGoxppAbTKG+gjeadoyZ0Duo43MbU6LmuJpTPCwk3WGFHqTyg9xiJbcIJSS2AtJkWG9R89Imgew8mI91zmcfQPfeo/D21iC9wdUZg2oaWoaG7xYvm59vFQ6qHt0EloQycb4WTN25cuttBFBKIRpfAsstkNpvD4Xtye9/802PLFi/6J1y6LXpx3mUQleJARHKCaGRbvWLZO1AwQEgUEBIFhOQWDRAS5UVIFOfinrheVHw2MTmFEwgJ1yAVxvFiKDBlaJA0uJmbrycEcw+3P0PTCDtOeJ1F8uKWCFL2fr5EOZzNOL+g0Qq9Lxz0IQQ7ceUKhSR2jzRxqb2Uj/MP46Ueb2WwyH1hREaPzln+HlFIjY1N+1NSzlirq/Wfg99/9saunVRszLaHdu3YHg32PueAOP4Klm8lk0JHt4GfZ6yPXE0tf2WxZCHZ7Q7K4XC667I77IuZC5nehIRzvBhqJD86s/KgM7CG7p4FUafh8pPsRAeFhu69SfWnjTgBisEi5aKDoQBjl7f9FSqgWBq/FPdVSIxIvTh/+Sok3OSI5kf7XbgvR/1yR2REIXV0dIRmX9beys7WljsdzhEeIQFBxFDLXl5E7doRMzFs+pTG+XNmFX726acPHo6Loz45fJhasmihG29CstraqfZ2+wCXyzWCZau+T0w63d9CQgcy6aACdRxDcJqKkJ9kp9Q9iK9tVGPyqQXgDkbg7wqCX6SgRmyAdmpo7w/JAyEk1Calj2WgYjOKXL8zsRKFBKNQA4hKp8+c62poaPwjfI0HLOfcX4WAYoqO2jQKLPVSdr++azsUkK9CagdCstnah14rvJ767XdHHSUlN64IhISbOdDO9IZYp4gNTIbGd7wCk1ch0jHodf4VJjGkHDig9nKYNLCDWSQN/3YD6hdWgl38JOLtpA9FTEg4f6JlqwX3pAoJTRMiUgZDKAP1HcyHTrgaYR4xIVFOp/PJgmuFFfngf52dnU+Q0nkDLuOsVitlb293Cwhib7dTFotlWloaU3s1vyANpHsUObVDHcISGt1XIWkIzpXSabhlli8zsD+oJdpGirRS/YIDd4LJeurCTX68WKQsqXA+E9qG+ho9FSSVIbwnVUgajB1olO8xEYgKCdLaaoouKv6hrNXYOt9ut8PlGAF3hMGWAa83NjVRNpDG4XDcwWg0rklLZ7iS0hufgXQDESHhliBCx3oDdUYBIR1LqAOtGxct0DqEHYd7eHg3hMRKbD9D8KvUZ3MqTFuFbVKI+AIdwDh/4soXTj5ouxkabyfJBl+E5G0f2isfUUjwD5RAzGbzQzW1dXOqdbphNbW1VE0NHp1OD6KOTVRI7UCIgusP6Gtq9iWnnOmqul0dhXkgi3M+BM5+pNOtELp7pvDWMRDcC4x8B6OzLzrgcLOssOPQAcuK2N0XIfXqVI9tqJB5+8Xa7Eu96IuwuP4Suyf0J85ejhYX0t2MSBTBHh4Vmp4opJYWgxujsZWqr2+ggJAoXY2eAoO/F/Ce1YYXkVBIMKKB5SJc0sGl3rC8/ALt2fNpzQ6HM9zVW0i4WVXoRP5ZjprufrbB0d0RBfccx0h3v8aCK1voWLTjOE+d/GsxJEeLzbAFdPdRMv/KUSwtfX+Es4ulex42kHzGd74Cc8/ouc8LXen5PV6QD62XEaRXENrrbVI00uIPvMWExHl8F0/37DeSDb4KieRHFpeeKCSDwegGCqmurt4tFn9E1CMigaWd52/jQX5fUlqakprOmMB/LzU3N+OEJNYgKc735agYfbPBl6f/pI5jfMgnNVr5UiYPuqxV+5CXFz4uAguFgFuKS53hSQj7UuzrD3x09LYXQ9vN0GQ/k8aOGpe+T0K6XV1NWaxWKYcNA1sMhgdANHLvgzo7u9zXK1n20PnzaVYQ8ZbB5SFBSPzszkp0vgLjEG+dyNL4iEBacvBovHQcFIeU42ZWpEP7KiTSS75qifmF/sS1lwc30H3pB1xkEgpJIZKfj5q4yOevkEjix054fgsJfu0BwkcZEqCs3zQ2Ne8pLin5urpad8hkaltQUnLjGbDfimQyLhjg298gDe7tb9Isoabx3wRV0/jXTvgBrfKkE+aLE8kjzCtcQvD5FB7UCLgyQgh288tTJSEfaVJB68QRQXt/N1GBaRuPmsY/OyP5UYov+DTCvBq65/JRCGq/AlM3tF+4xBSzQYncw7VPCOlhff8ICQqotq7OfRghWKphMZstaxKTUywnTp5qPHP2vOn0mXNcKpNhPpWYxKWmpjeDZd0WtG4vjZORuRcoafEI2QO/hASXdAajUcozpEGF14uPpgPhWK22xRaLdUbV7eo3b9ws28+yVXsdDvtceHonC0nmPoShey89ien9jkjNLQaqrc1MxASw2donpaZn1JeVlyeBfdEv2232O/sjMe4DJ8r8+GDo7i8K4va1KrH8PgsJPkuC+yL4tgL8JAGPucvKK2MzM7PaWltbl4AyB/wvj10Wksz9CCeCaDSC+CQkGInq6utF90Q8oIzf5l0tuFheXvkPsI962HN6JwtJ5n6FofEiwn3hsxeShVQF9kVQRPDfSZKwN6Kampt3Xiu83mQymcL5a/BrE1BMspBk7kNUdO8TVeGJoCiShOR+DaiuTvKfFQbpHqmoqMzW6/WJ8PgbOQ6XkQlKsBd5IUFaDAbJkQhitdpWgKUg226zLYS/y0KS+TGAvdjc3OKmqamFamtroywWq+gpHY/ZbBnU3GL4FHx+A8r5BeEhrYxM0BFwA2RkgoGAGyAjEwwE3AAZmWAg4AbIyAQDATdARiYYCLgBMjLBQMANkJEJBgJugIxMMPBfChd6NRZ5pkMAAAAASUVORK5CYII="}, {"name":"file name", "description":"description", "file":"iVBORw0KGgoAAAANSUhEUgAAANIAAAAzCAYAAADigVZlAAAQN0lEQVR4nO2dCXQTxxnHl0LT5jVteHlN+5q+JCKBJITLmHIfKzBHHCCYBAiEw+I2GIMhDQ0kqQolIRc1SV5e+prmqX3JawgQDL64bK8x2Ajb2Bg7NuBjjSXftmRZhyXZ1nZG1eL1eGa1kg2iyua9X2TvzvHNN/Ofb2Z2ZSiO4ygZGZm+EXADZGSCgYAbICMTDATcABmZYCDgBsjIBAMBN0BGJhgIuAEyMsGA1wQdHZ1UV1cX5XK5qM7OzgcMRuNTrSbTEraq6strhdfzruTk5Wpz8q5c1l7Jyb6szc3K1l7RggtFxcWX2dvVB02mtmVOp3NIV2fnQFie2WyB5QS84TIy/YnXBFBI8BMM/pDqat0XzIVM08lTSVxyytn6jAuZV4FuzmtzclJz8/LT8vML0nJzr54HYkpLS88oTkxMMZ48mchlXrxUX1ffcBCUM8xms8lCkgk6pCT6aZvZvCrzYpbu2PfxHAg8l+obGmOt1vaJQBAPkvI5nM5fWyyWWTU1tfuA+IqOHDvGgehVCK4pA91oGZn+xluCAc0thtj4hCT72XOp9S0thi2FBQWPvb13z9RN61QH5s8NYxbMDct7KXyudt7MGeeWLFrwn8iVKz7auDZy3Z7dbzz91p43B8ZsjYLlDKmprd3/ffwpLjWNqbW32xcFuuEyMv2J2M1BJpMpKiExxZKZeamira1tvvqdt8OWL1l8asq4kNbRzz7NTRo7uuMPo4Y7Rz/zFBc64lluzHNDuZFDFe5PICx25/aY2B3bogf/dd9fKCA+CuytohOSkjuyLmtLXRwXGujGy8j0F8Qbdrt9bDpzQQ8jSHl5+dLt0VsOThgzwj7i6Se5kOHDuIljR9mXRrykjZj/wlVeSONHP8+FhykrJoeOsY8aNoQLAYJa9erShIPvvRsKhQTK/YleX3Pw5KlErpKt+iLQjZeR6S9IN35VXl75r3gw4HU6/Z6ojes/gMKAUQiKBQKiUvvLC1/MXL18WcKsaZOrJ4WObly7euUJsOQ7FjZ9Sh2IVC4oLhihZk6d1LB5/dpt+9R/hnuq4Xl5VwvT0jLKXS7XOHgaCAm0I2Rk+gL2os1mewXsiUw5uXlZn8T9LVI5ZWI1jEQTxozkgECgkDrmKqfrFy8ILwJ7om+3bNoQumTRwtDoqE0fTBsf2ggwg+jVBdOCT7eYwGfnti2bQXA6ME2nr9mbnHLOWV/fEI3WTdO0jMzdZjBAKWBwX8ojCqm8vOJoYvLp9qPfHTmy5rXlJ+BSbtzI5+5EI4ALRCTHHHpaQ8zWqOidO2IooBAKRKRDQDwGevJ4w8SQUR0e0bmB0QxEKh2IYsdbTW0zmIxM4/Wi4q9BfQMkCikCoAEUADgEeI3xOOVedkicp14e1V2uLwSpTwxNAPwRaGC7OQFqQp9xGDT+1ksUUubFrMoLFy/VL5g7+4ep48fa+P0Pz9jnn4H7JCcQBbP79V1rgJDmASE9um7NqvmxMdFbVateiwd7KKswHx+dwBKwzGq1jgDRrjQ7W5sB6hvsRUhQQCyh8Sg4xwW64/oTpUQ/CIm7xz652yg9flb40R+xIn5i/LWJKKSk5NOuwqIi7cSQkXooAD6ywE8YneDyLWrDuq/WR67+BvxcB5dtG9dGHgF7oZsgSuWFz555c0LISKcwIvHlAHSdnR0P37h5699pzIW6NrNlptFoIglJ7cOAgcTf40711nH3g5AguEH3/4YGaZPSj/6Ix/hGmKd/hXQqIanz5q1b8WA5VwOXdLwgoIjAsk2/Y1v0odUrXj0OT+vgNSCkjgXzZleANF3wpI6PRALxcDDt7BlTby+NWPgdqOPBisrKz8E+zFFXX79Sp9fjhKQiDAqjx6kRHmfCdHDWZek+zCp+gnac6i7XhxOSUkAExiZI7D32y73wtbKfy/CnPDdEISUkJjsrKiqPhocp86ZPGGeDSzkIWJa1Rq5ccXyDas1X8PBBuG9Cow8UE/yEaYYPeZybPnFcM1gGRh/6+KNhNbV1o7Mua29dysrOdblcQ4SvDHmMg5s/I2ZAxNP+bQz5zaVaABz0ij7kh6D7NVJnwL1NLJLXn47DCQmXjkXSqAnpFB4/CO2KkODjEE861B9i7VcKwPldgaQJQfKi4yFWkNZbPXzZuP4iQRobaLrBIhEpubP0xq2E9989MHnLpg3rX5hFlz3/1BMcWLaVRm/eeIieNL4KRhi450EjDxQOvAf2T+mrli9bDZaAq3Zu37b3nbf2zvnwg/d/DoRENbcYRmhzcn84n5peDkQ0FbNHUmMGjD/LtsGesnCi5GEEnYbLH+clP9ox6ABiRdKzmDz9ISR0wKgx7WJE7ILtxUUxlQQfGDFtQutC7cH1OUPIi8NbPWjZUtBgbIzApFMQhZSccrbrav61zAqWfWR79JbJ8+eG5Q97/HccfB0I/P4eEJADRigoJP6NBvgzBC715s2coTuwf9+0qI3rKbB3ooCQKCAkCgiJgkKCS7uWFuMbiUkpjpzcvCvg9yGIkFicwZiGeRMR7oQPB+x8VEy+5OcRDiDcoCdBErI/QsINdmH5pGiPAxUT6cQLxYjkY5D7aozdaiQNQ8iLoz+EhPY1i7FRg7ORKKTUtHSdVptTarPZhr737oFHgRj+7lmeVcRsjfrwxdkzc+DSDj50VU6Z0LR5/drDK5a8HLt4QfhusAfaBUQz8tDHHw/atE5FEhLkods6/ZfHjsdzZWXlJwRCGoxppAbTKG+gjeadoyZ0Duo43MbU6LmuJpTPCwk3WGFHqTyg9xiJbcIJSS2AtJkWG9R89Imgew8mI91zmcfQPfeo/D21iC9wdUZg2oaWoaG7xYvm59vFQ6qHt0EloQycb4WTN25cuttBFBKIRpfAsstkNpvD4Xtye9/802PLFi/6J1y6LXpx3mUQleJARHKCaGRbvWLZO1AwQEgUEBIFhOQWDRAS5UVIFOfinrheVHw2MTmFEwgJ1yAVxvFiKDBlaJA0uJmbrycEcw+3P0PTCDtOeJ1F8uKWCFL2fr5EOZzNOL+g0Qq9Lxz0IQQ7ceUKhSR2jzRxqb2Uj/MP46Ueb2WwyH1hREaPzln+HlFIjY1N+1NSzlirq/Wfg99/9saunVRszLaHdu3YHg32PueAOP4Klm8lk0JHt4GfZ6yPXE0tf2WxZCHZ7Q7K4XC667I77IuZC5nehIRzvBhqJD86s/KgM7CG7p4FUafh8pPsRAeFhu69SfWnjTgBisEi5aKDoQBjl7f9FSqgWBq/FPdVSIxIvTh/+Sok3OSI5kf7XbgvR/1yR2REIXV0dIRmX9beys7WljsdzhEeIQFBxFDLXl5E7doRMzFs+pTG+XNmFX726acPHo6Loz45fJhasmihG29CstraqfZ2+wCXyzWCZau+T0w63d9CQgcy6aACdRxDcJqKkJ9kp9Q9iK9tVGPyqQXgDkbg7wqCX6SgRmyAdmpo7w/JAyEk1Calj2WgYjOKXL8zsRKFBKNQA4hKp8+c62poaPwjfI0HLOfcX4WAYoqO2jQKLPVSdr++azsUkK9CagdCstnah14rvJ767XdHHSUlN64IhISbOdDO9IZYp4gNTIbGd7wCk1ch0jHodf4VJjGkHDig9nKYNLCDWSQN/3YD6hdWgl38JOLtpA9FTEg4f6JlqwX3pAoJTRMiUgZDKAP1HcyHTrgaYR4xIVFOp/PJgmuFFfngf52dnU+Q0nkDLuOsVitlb293Cwhib7dTFotlWloaU3s1vyANpHsUObVDHcISGt1XIWkIzpXSabhlli8zsD+oJdpGirRS/YIDd4LJeurCTX68WKQsqXA+E9qG+ho9FSSVIbwnVUgajB1olO8xEYgKCdLaaoouKv6hrNXYOt9ut8PlGAF3hMGWAa83NjVRNpDG4XDcwWg0rklLZ7iS0hufgXQDESHhliBCx3oDdUYBIR1LqAOtGxct0DqEHYd7eHg3hMRKbD9D8KvUZ3MqTFuFbVKI+AIdwDh/4soXTj5ouxkabyfJBl+E5G0f2isfUUjwD5RAzGbzQzW1dXOqdbphNbW1VE0NHp1OD6KOTVRI7UCIgusP6Gtq9iWnnOmqul0dhXkgi3M+BM5+pNOtELp7pvDWMRDcC4x8B6OzLzrgcLOssOPQAcuK2N0XIfXqVI9tqJB5+8Xa7Eu96IuwuP4Suyf0J85ejhYX0t2MSBTBHh4Vmp4opJYWgxujsZWqr2+ggJAoXY2eAoO/F/Ce1YYXkVBIMKKB5SJc0sGl3rC8/ALt2fNpzQ6HM9zVW0i4WVXoRP5ZjprufrbB0d0RBfccx0h3v8aCK1voWLTjOE+d/GsxJEeLzbAFdPdRMv/KUSwtfX+Es4ulex42kHzGd74Cc8/ouc8LXen5PV6QD62XEaRXENrrbVI00uIPvMWExHl8F0/37DeSDb4KieRHFpeeKCSDwegGCqmurt4tFn9E1CMigaWd52/jQX5fUlqakprOmMB/LzU3N+OEJNYgKc735agYfbPBl6f/pI5jfMgnNVr5UiYPuqxV+5CXFz4uAguFgFuKS53hSQj7UuzrD3x09LYXQ9vN0GQ/k8aOGpe+T0K6XV1NWaxWKYcNA1sMhgdANHLvgzo7u9zXK1n20PnzaVYQ8ZbB5SFBSPzszkp0vgLjEG+dyNL4iEBacvBovHQcFIeU42ZWpEP7KiTSS75qifmF/sS1lwc30H3pB1xkEgpJIZKfj5q4yOevkEjix054fgsJfu0BwkcZEqCs3zQ2Ne8pLin5urpad8hkaltQUnLjGbDfimQyLhjg298gDe7tb9Isoabx3wRV0/jXTvgBrfKkE+aLE8kjzCtcQvD5FB7UCLgyQgh288tTJSEfaVJB68QRQXt/N1GBaRuPmsY/OyP5UYov+DTCvBq65/JRCGq/AlM3tF+4xBSzQYncw7VPCOlhff8ICQqotq7OfRghWKphMZstaxKTUywnTp5qPHP2vOn0mXNcKpNhPpWYxKWmpjeDZd0WtG4vjZORuRcoafEI2QO/hASXdAajUcozpEGF14uPpgPhWK22xRaLdUbV7eo3b9ws28+yVXsdDvtceHonC0nmPoShey89ien9jkjNLQaqrc1MxASw2donpaZn1JeVlyeBfdEv2232O/sjMe4DJ8r8+GDo7i8K4va1KrH8PgsJPkuC+yL4tgL8JAGPucvKK2MzM7PaWltbl4AyB/wvj10Wksz9CCeCaDSC+CQkGInq6utF90Q8oIzf5l0tuFheXvkPsI962HN6JwtJ5n6FofEiwn3hsxeShVQF9kVQRPDfSZKwN6Kampt3Xiu83mQymcL5a/BrE1BMspBk7kNUdO8TVeGJoCiShOR+DaiuTvKfFQbpHqmoqMzW6/WJ8PgbOQ6XkQlKsBd5IUFaDAbJkQhitdpWgKUg226zLYS/y0KS+TGAvdjc3OKmqamFamtroywWq+gpHY/ZbBnU3GL4FHx+A8r5BeEhrYxM0BFwA2RkgoGAGyAjEwwE3AAZmWAg4AbIyAQDATdARiYYCLgBMjLBQMANkJEJBgJugIxMMPBfChd6NRZ5pkMAAAAASUVORK5CYII="}],


    def _validate_object(self, json_object, response):
        response['valide'] = True
        if not 'subject_id' in json_object:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'subject_id is missing'} 
        else:
            try:
                json_object['subject_id'] = int(json_object['subject_id'])
            except ValueError:
                response['valide'] = False
                response['success'] = False
                response['error'] = {'code':404, 'message':'subject_id must be of type integer'}

        if not 'class_id' in json_object:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'class_id is missing'} 
        else:
            try:
                json_object['class_id'] = int(json_object['class_id'])
            except ValueError:
                response['valide'] = False
                response['success'] = False
                response['error'] = {'code':404, 'message':'class_id must be of type integer'} 

        if not 'timing_system_periode_id' in json_object:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'timing_system_periode_id is missing'} 
        else:
            try:
                json_object['timing_system_periode_id'] = int(json_object['timing_system_periode_id'])
            except ValueError:
                response['valide'] = False
                response['success'] = False
                response['error'] = {'code':404, 'message':'timing_system_periode_id must be of type integer'}

        if not 'student_ids' in json_object:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'student_ids is missing'} 


        class_id = request.env['school.class'].search([('id','=',json_object['class_id'])])
        if not class_id:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given ID'} 
        else:
             json_object['grade_id']= class_id.grade_id.id
        
        return response


       

    @http.route('/api/teacher/task', type='http', auth="user", methods=['GET'],  csrf=False)
    def teacher_current_task(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:            
            user = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
            session = self._getCurrentSession(user)
            json_session = False
            if session:
                json_session = {
                        'id':session.id,
                        'periode':{
                            'id':session.timing_periode.id,
                            'name':session.timing_periode.name,
                        },
                        'class':{
                            'id':session.class_id.id,
                            'name':session.class_id.name,
                        },
                        'subject':{
                            'id':session.subject_id.id,
                            'name':session.subject_id.name,
                        },
                        'teacher':{
                            'id':session.teacher_id.id,
                            'name':session.teacher_id.name,
                        },
                        'classroom':{
                            'id':session.classroom_id.id,
                            'name':session.classroom_id.name,
                        },
                        'students':self._get_students_by_class_no_image(session.class_id.id),
                        'start_time':session.start_time,
                        'end_time':session.end_time,
                        'day_of_week':session.day_of_week,
                    }

        response['success'] = True
        response['data'] = json_session
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)




    @http.route('/api/teacher/task/<int:class_id>/class', type='http', auth="user", methods=['GET'],  csrf=False)
    def teacher_task_by_class(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []

        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:            
            teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
            tasks = request.env['school.task'].search(['&',('class_id','=',class_id), ('teacher_id','=',teacher.id)])
            task_list = []
            for task in tasks:
                file_list = []
                students_list = []
                for student in task.student_ids:
                    students_list.append({
                        'id':student.id,
                        'name':student.name,
                        'last_name':student.last_name,
                        })
                for file in task.file_ids:
                    domain = [
                        ('res_model', '=', file._name),
                        ('res_field', '=', 'file'),
                        ('res_id', '=', file.id),
                    ]
                    attachment = request.env['ir.attachment'].search(domain)
                    base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                    url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+file.file_name
                    url = url.replace(" ", "_")
                    _logger.warning('#### URL of document %s',url)
                    file_list.append({
                            'id':file.id,
                            'description':file.description,
                            'content_type':file.content_type,
                            'file_name':file.file_name,
                            'file':file.file,
                            'url':url,
                        })
                json_object = {
                    'id':task.id,
                    'name':task.name,
                    'deadline':task.dead_line,
                    'creation_date':task.creation_date,
                    'state':task.state,
                    'description':task.description,
                    'all_student':task.all_student,
                    'grade':{
                        'id':task.grade_id.id,
                        'name':task.grade_id.name,
                    },
                    'class':{
                        'id':task.class_id.id,
                        'name':task.class_id.name,
                    },
                    'subject':{
                        'id':task.subject_id.id,
                        'name':task.subject_id.name,
                    },
                    'teacher':{
                        'id':task.teacher_id.id,
                        'name':task.teacher_id.name,
                    },
                    'periode':{
                        'id':task.timing_system_periode_id.id,
                        'name':task.timing_system_periode_id.name,
                    },
                    'files':file_list,
                    'students':students_list,
                }
                task_list.append(json_object)

        response['success'] = True
        response['data'] = task_list
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
    

    # {
    #         academic_year_id:1
    #         all_student:true
    #         class_id:1
    #         dead_line:"2017-09-25"
    #         description:"description of task"
    #         educational_stage_id:2
    #         file_ids:[]
    #         grade_id:5  
    #         name:"test"
    #         state:"unapproved"
    #         student_ids:[[1, 177, {age: 7, last_name: "Mabrouk ", pid: "20170177", state: "pre-registred", name: "Molka "}],…]
    #         subject_id:1
    #         teacher_id:6
    #         timing_system_id:2
    #         timing_system_periode_id:2
    # }


    def _getCurrentSession(self,user_id):
        dow = datetime.today().weekday()
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        for session in request.env['time.table.session'].search([('time_table_id.academic_year','=',current_academic_year.id)]):# <--- check current year and timing session period here
            start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                print "#### CURRENT PERIODEEE ",session.timing_periode.name
                # try to check day of week and teacher first to avoid unnecessary further test of datetime
                if session.day_of_week == dow and session.teacher_id.id == user_id.id: # user.id is the hr.employee related to logged user
                    session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
                    session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
                    print "SESSION START TIME",session_start_time
                    print "CURRENT TIME ",current_time
                    print "SESSION END TIME ",session_end_time
                    if session_start_time <= current_time <= session_end_time:
                        return session
            else:
                print "#### NOT CURRENT PERIODE ",session.timing_periode.name
            print "--------------------------------"
        return False

    
    @http.route('/api/teacher/<int:teacher_id>/timetable', type='http', auth="user", methods=['GET'],  csrf=False)
    def teacher_timetable(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:            
            user = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
            session_list = self._get_session(user)
            data = session_list

        response['success'] = True
        response['data'] = data
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)


    @http.route('/api/teacher/subjects', type='http', auth="user", methods=['GET'],  csrf=False)
    def teacher_subject(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:            
            teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
            subject_list = []
            for rec in teacher.subject_ids:
                subject = {
                    'name':rec.name,
                    'code':rec.code,
                    'is_practical':rec.is_practical
                }
                subject_list.append(subject)
        response['success'] = True
        response['data'] = subject_list
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)



    @http.route('/api/teacher/class', type='http', auth="user", methods=['GET'],  csrf=False)
    def teacher_class(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:            
            user = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
            class_list = self._get_classes(user)
            data = class_list

        response['success'] = True
        response['data'] = data
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)


    def _get_session(self,user_id):
        session_list = []
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        for session in request.env['time.table.session'].search([('time_table_id.academic_year','=',current_academic_year.id)]):# <--- check current year and timing session period here
            start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                print "#### CURRENT PERIODE ",session.timing_periode.name
                # try to check day of week and teacher first to avoid unnecessary further test of datetime
                if session.teacher_id.id == user_id.id: # user.id is the hr.employee related to logged user
                    session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
                    session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
                    print "SESSION START TIME",session_start_time
                    print "CURRENT TIME ",current_time
                    print "SESSION END TIME ",session_end_time
                    json_session = {
                        'class':session.class_id.name,
                        'subject':session.subject_id.name,
                        'teacher':session.teacher_id.name,
                        'classroom':session.classroom_id.name,
                        'start_time':session.start_time,
                        'end_time':session.end_time,
                        'day_of_week':session.day_of_week,
                    }
                    session_list.append(json_session)
            else:
                print "#### NOT CURRENT PERIODE ",session.timing_periode.name
            print "--------------------------------"
        return session_list

   
    def _get_classes(self,user_id):
        class_list = []
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        for session in request.env['time.table.session'].search([('time_table_id.academic_year','=',current_academic_year.id)]):# <--- check current year and timing session period here
            start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                print "#### CURRENT PERIODE ",session.timing_periode.name
                # try to check day of week and teacher first to avoid unnecessary further test of datetime
                if session.teacher_id.id == user_id.id: # user.id is the hr.employee related to logged user
                    session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
                    session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
                    print "SESSION START TIME",session_start_time
                    print "CURRENT TIME ",current_time
                    print "SESSION END TIME ",session_end_time
                    json_class = {
                        'id':session.class_id.id,
                        'name':session.class_id.name,
                        'grade_name':session.class_id.grade_id.name,
                        'educational_stage':session.class_id.grade_id.educational_stage_id.name,
                    }
                    print "### class object ",json_class
                    add = True
                    for item in class_list:
                        if item['id'] == json_class['id']:
                            add = False
                            break
                    if add:
                        class_list.append(json_class)
            else:
                print "#### NOT CURRENT PERIODE ",session.timing_periode.name
            print "--------------------------------"
        return class_list


    @http.route('/api/teacher/class/<int:class_id>/student', type='http', auth="user", methods=['GET'],  csrf=False)
    def class_student(self,class_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        print "########################################################## STUDENT LIST WS"
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:
            class_list = self._get_students_by_class(class_id)
            data = class_list

        response['success'] = True
        response['data'] = data
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)



    def _get_students_by_class(self, class_id):
        student_list  =  []
        students = request.env['school.student'].search([('class_id','=',class_id)])
        for student in students:
            domain = [
                    ('res_model', '=',student.user_id.partner_id._name),
                    ('res_field', '=', 'image'),
                    ('res_id', '=',student.user_id.partner_id.id),
                    ]
            attachment = request.env['ir.attachment'].search(domain)
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+'?session_id='+request.env['ir.http'].session_info()['session_id']
            url = url.replace(" ", "_")
            _logger.warning('#### URL of document %s',url)
            std = {'id':student.id, 'name': student.name,'last_name':student.last_name,'birth_date':student.birth_date,'image':url}
            student_list.append(std)
        return student_list


    def _get_students_by_class_no_image(self, class_id):
        student_list  =  []
        students = request.env['school.student'].search([('class_id','=',class_id)])
        for student in students:
            std = {'id':student.id, 'name': student.name,'last_name':student.last_name}
            student_list.append(std)
        return student_list


    @http.route('/api/teacher/<int:class_id>/students', type='http', auth="user", methods=['GET'],  csrf=False)
    def class_student_trombinoscope(self,class_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        else:
            class_list = self._get_students_trombinoscope_by_class(class_id)
            data = class_list

        response['success'] = True
        response['data'] = data
        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)

    def _get_students_trombinoscope_by_class(self, class_id):
        student_list  =  []
        students = request.env['school.student'].search([('class_id','=',class_id)])
        for student in students:
            std = {'id':student.id, 'name': student.name,'last_name':student.last_name, 'birthday':student.birth_date, 'photo':student.image, 'class':student.class_id.name}
            student_list.append(std)
        return student_list