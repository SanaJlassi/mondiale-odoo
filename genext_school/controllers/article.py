# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class SchoolArticle(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }

        


    @http.route('/api/article', type='http', auth="user", methods=['GET','POST'],  csrf=False)
    def articles(self,offset=0, limit=None, **fields):
        response = {'success':False, 'data':None}
        if request.httprequest.method == 'GET':
            if limit is not None:
                try: 
                    offset = int(offset)
                    limit = int(limit)
                except ValueError:
                    print "## LIMIT IS NOT INT NEITHER None"

            if isinstance(offset, int) and isinstance(limit, int) and offset>=0 and limit>0:
                print "#### PAGINATION OFFSET", offset
                print "#### PAGINATION LIMIT", limit
                data = self._get_articles(int(offset), int(limit))
            else:
                print "#### NO PAGINATION OFFSET", offset
                print "#### NO PAGINATION LIMIT", limit
                data = self._get_articles()

            response['data'] = data
            response['success'] = True
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)

        else:
            if not request.env.user.has_group('genext_school.group_school_teacher'):
                response['success'] = False
                response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)
                           
            teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
            _logger.warning('### DATAAAAAAA :  (%s).', request.params.get('article',False))
            json_object = None
            if not 'article' in request.params:
                response['success'] = False
                response['error'] = {'code':404, 'message':'article object not submited'} 
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)

            if not request.params.get('article',False):
                response['success'] = False
                response['error'] = {'code':404, 'message':'value of article key is missing'}
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)

            try:
                json_object = json.loads(request.params.get('article',False))
            except ValueError, e:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Unable to deserialize article object'} 
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)

            if not 'title' in json_object:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Title is missing'}
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)                

            if not 'content' in json_object:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Content of the article is missing'}
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)


            article = {
                'name':json_object['title'],
                'subtitle':json_object['subtitle'] if 'subtitle' in json_object else False,
                'content':json_object['content'],
                'from_teacher':True,
                'image_ids':self._prepar_file_objects(json_object['images']) if 'images' in json_object else False
            }

            res = request.env['school.article'].create(article)

            response['data'] = len(res)
            response['success'] = True
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)


    def _prepar_file_objects(self, files):
        file_list = []
        for file in files:
            base64 = file['base64'].split("base64,")
            if len(base64):
                file_list.append((0,False,{'name':file['name'],'file_name':file['name'], 'description':"",'image':base64[1]}))
        if len(file_list):
            return file_list
        else:
            return False

    def decode_base64(self, data):
        missing_padding = len(data) % 4
        if missing_padding != 0:
            data += '='* (4 - missing_padding)
        return data

        
    @http.route('/api/article/<int:id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def articleById(self, id, **fields):
        response = {'success':False, 'data':None}
        if id >=0:
            data = self._get_articleById(id)
        response['data'] = data
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)


    @http.route('/api/article/<int:id>/like', type='http', auth="user", methods=['POST'],  csrf=False)
    def articleLike(self, id, **fields):
        response = {'success':False, 'data':None}
        print "FROM LIKE METHODE"
        print "### METHOD ",request.httprequest.method
        article = request.env['school.article'].search([('id','=',id)])
        # check if the article exists
        if not article or len(article) != 1:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No article found with the id '+str(id)}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)
        if request.httprequest.method == 'POST':
            return self._add_like(article)
        elif request.httprequest.method == 'DELETE':
            return self._unlike(article)


    @http.route('/api/article/<int:id>/dislike', type='http', auth="user", methods=['POST'],  csrf=False)
    def articleDislike(self, id, **fields):
        response = {'success':False, 'data':None}
        
        article = request.env['school.article'].search([('id','=',id)])
        # check if the article exists
        if not article or len(article) != 1:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No article found with the id '+str(id)}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)
        return self._unlike(article)


    @http.route('/api/article/<int:id>/comment', type='http', auth="user", methods=['POST'],  csrf=False)
    def articleAddComment(self, id, **fields):
        response = {'success':False, 'data':None}
        if not request.params.get('comment',False):
            response['error'] = {'code':404, 'message':'Comment must not be empty'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)

        if id:
            try: 
                id = int(id)
            except ValueError:
                response['error'] = {'code':404, 'message':'Inavlide id: '+str(id)}
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)
        

        article = request.env['school.article'].search([('id','=',id)])
        if not article or len(article) != 1:
            response['error'] = {'code':404, 'message':'No article found with the id '+str(id)}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)

        comment = {
            'name':request.params['comment'],
            'user_id': request.env.user.id,
            'article_id':id
            }

        comment_id = request.env['school.article.comment'].create(comment)
        if not comment_id or not len(comment_id):
            response['error'] = {'code':404, 'message':'Unkown error occured while adding comment'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)
        comment = request.env['school.article.comment'].browse(comment_id.id)

        comment_json = {
            'id':comment.id,
            'name':comment.name,
            'user_id': comment.user_id.id,
            'article_id':comment.article_id.id,
            'datetime':comment.datetime,
            }

        response = {'success':True, 'data':comment_json}
        return http.request.make_response(json.dumps(response),SchoolArticle.HEADER)

    @http.route('/api/article/<int:idArticle>/comment/<int:idComment>/delete', type='http', auth="user", methods=['POST'],  csrf=False)
    def articleDeleteComment(self, idArticle, idComment, **kwargs):
        return self._delete_comment(idArticle,idComment)

    @http.route('/api/article/<int:idArticle>/comment/<int:idComment>', type='http', auth="user", methods=['POST'],  csrf=False)
    def articleUpdateComment(self, idArticle, idComment, **kwargs):
        return self._update_comment(idArticle,idComment)


    def _get_articleById(self, id):
        school_groups = [
                         'genext_school.group_school_administration',
                         'genext_school.group_school_teacher',
                         'genext_school.group_school_student',
                         'genext_school.group_school_parent'
                        ]
        user_groups = []
        for group in school_groups:
            if request.env.user.has_group(group):
                user_groups.append(request.env.ref(group).id)   
     
        domain = ['&',('group_ids','in',user_groups),('state','=','published'), ('id','=',id)]
        article = request.env['school.article'].search(domain)
        articles = []
        if not article or len(article) != 1:
            response['error'] = {'code':404, 'message':'No article found with the id '+str(id)}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)

        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
        article.sudo().increment_view()
        article_obj = {
            'id':article.id,
            'title':article.name,
            'subtitle':article.subtitle,
            'content':article.content,
            'datetime':article.datetime,
            'views_nbr':article.views_nbr,
            'like_count':article.like_count,
            'comment_count':article.comment_count,
            'author':{
                'id':article.user_id.id, 
                'name':article.user_id.name,
                'image':base_url+'/web/image?model='+article.user_id._name+'&id='+str(article.user_id.id)+'&field=image',
            },
            'images':self._get_images(article),
            'comments':self._get_comments(article),
            'likes':self._get_likes(article)
        }
        return article_obj



    def _get_articles(self, offset=0, limit=None, order='create_date desc'):
        school_groups = [
                         'genext_school.group_school_administration',
                         'genext_school.group_school_teacher',
                         'genext_school.group_school_student',
                         'genext_school.group_school_parent'
                        ]
        user_groups = []
        for group in school_groups:
            if request.env.user.has_group(group):
                user_groups.append(request.env.ref(group).id) 


        partner_id = request.env.user.partner_id
        

        class_ids = []

        if partner_id.is_school_parent:
            student_ids = partner_id.student_ids
            class_ids = [student.class_id.id for student in student_ids]
            print "################ CLASS IDS PARENT",class_ids
            _logger.warning('########### CLASS IDS PARENT %s',class_ids)
        elif partner_id.is_school_teacher:
            hr_employee_id = request.env['hr.employee'].sudo().search([('partner_id','=',partner_id.id)])
            class_ids = hr_employee_id.class_ids.ids
            # class_ids = [student.class_id.id for student in student_ids]
            print "################ CLASS IDS FROM TEACHER ",class_ids            
            _logger.warning('########### CLASS IDS TEACHER %s',class_ids)
        domain = ['&',('group_ids','in',user_groups),('state','=','published')]
        records = request.env['school.article'].search(domain,offset, limit, order)
        articles = []
        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
        print "### GETTING ARTICLES "
        for rec in records:
            append = False
            for id in class_ids:
                if id in rec.class_ids.ids:
                    append = True
                    break
            if append:
                rec.sudo().increment_view()
                article = {
                    'id':rec.id,
                    'title':rec.name,
                    'subtitle':rec.subtitle,
                    'content':rec.content,
                    'datetime':rec.datetime,
                    'views_nbr':rec.views_nbr,
                    'like_count':rec.like_count,
                    'comment_count':rec.comment_count,
                    'author':{
                        'id':rec.user_id.id, 
                        'name':rec.user_id.name,
                        'image':base_url+'/web/image?model='+rec.user_id._name+'&id='+str(rec.user_id.id)+'&field=image',
                    },
                    'images':self._get_images(rec),
                    'comments':self._get_comments(rec),
                    'likes':self._get_likes(rec)
                }
                self._get_images(rec)
                articles.append(article)
        return articles

    def _get_comments(self, article_id, offset=0, limit=None):
        domain = [('article_id','=',article_id.id)]
        records = request.env['school.article.comment'].search(domain, offset, limit)
        comments = []
        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
        for record in records:
            comment = {
                        'id':record.id,
                        'name':record.name, 
                        'user':{
                            'id':record.user_id.id, 
                            'name':record.user_id.name,
                            'image':base_url+'/web/image?model='+record.user_id._name+'&id='+str(record.user_id.id)+'&field=image',
                        },
                        'article_id': record.article_id.id,
                        'datetime': record.datetime
                        }
            comments.append(comment)
        return comments

    def _update_comment(self, idArticle, idComment):
        response = {'success':False, 'data':None}
        # check if the comment body is empty 
        if not request.params.get('comment',False):
            response['error'] = {'code':404, 'message':'Comment must not be empty'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_PUT)
        
        comment = request.env['school.article.comment'].search([('id','=',idComment)])
        # check if comment with the given ID exists
        if not comment:
            response['error'] = {'code':404, 'message':'No comment found with the given id '+str(idComment)}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_PUT)    
        # check if the comment belongs to the logged user and the article related to the comment
        if request.env.user.id != comment.user_id.id or comment.article_id.id != idArticle:
            response['error'] = {'code':404, 'message':'You can only update your comments'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_PUT)  


        result = comment.write({'name':request.params.get('comment',''), 'datetime': fields.Datetime.now()})
        if not result:
            response['error'] = {'code':404, 'message':'Unkown error occured while updating comment'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_PUT)
        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
        comment_json = {
                    'id':comment.id,
                    'name':comment.name, 
                    'user':{
                        'id':comment.user_id.id, 
                        'name':comment.user_id.name,
                        'image':base_url+'/web/image?model='+comment.user_id._name+'&id='+str(comment.user_id.id)+'&field=image',
                    },
                    'article_id': comment.article_id.id,
                    'datetime': comment.datetime
                }

        response = {'success':True, 'data':comment_json}
        return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_PUT)

    def _delete_comment(self, idArticle, idComment,):
        response = {'success':False, 'data':None}
        comment = request.env['school.article.comment'].search([('id','=',idComment)])
        # check if comment with the given ID exists
        if not comment:
            response['error'] = {'code':404, 'message':'No comment found with the given id '+str(idComment)}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_DELETE)    
        # check if the comment belongs to the logged user and the article related to the comment
        if request.env.user.id != comment.user_id.id or comment.article_id.id != idArticle:
            response['error'] = {'code':404, 'message':'You can only delete your comments'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_DELETE)  

        result = comment.unlink()
        if not result:
            response['error'] = {'code':404, 'message':'Unkown error occured while deleting comment'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_DELETE)
        response = {'success':True, 'data':None}
        return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_DELETE)

    def _get_images(self, article_id):
        records = request.env['school.article.image'].search([('article_id','=',article_id.id)])
        images = []
        for record in records:
            image = {
                'id':record.id,
                'name': record.name,
                'description': record.description,
                'article_id': record.article_id.id,
                'url': record.image_url
                }
            images.append(image)
            print "## IMAGE ",image
        return images

    def _get_likes(self, article_id):
        records = request.env['school.article.like'].search([('article_id','=',article_id.id)])
        likes = []
        for record in records:
            like = {
                    'user_id':record.user_id.id, 
                    'name':record.user_id.name
                    }
            likes.append(like)
        return likes

    def _unlike(self, article_id):
        response = {'success':False, 'data':None}
        like = request.env['school.article.like'].search(['&',('article_id','=',article_id.id), ('user_id','=',request.env.user.id)])
        if not like or len(like) != 1:
            response['error'] = {'code':404, 'message':"logged user didn't like the article with the id "+str(article_id.id)}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_DELETE)
        else:
            result = like.unlink()
            if not result:
                response['error'] = {'code':404, 'message':'Unkown error occured while deleting like'}
                return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_DELETE)   
        response = {'success':True, 'data':None}
        return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_DELETE)   

    def _add_like(self, article_id):
        response = {'success':False, 'data':None}
        like = request.env['school.article.like'].search(['&',('article_id','=',article_id.id),('user_id','=',request.env.user.id)])
        # check if logged user already liked the article
        if like:
            response['error'] = {'code':404, 'message':'You cannot like the same article twice'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)

        result = article_id.write({'like_ids':[(0, 0, {'name':'like','user_id':request.env.user.id, 'article_id':article_id.id})]})
        if not result:
            response['error'] = {'code':404, 'message':'Unkown error occured while adding like'}
            return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)
        response = {'success':True, 'data':None}
        return http.request.make_response(json.dumps(response),SchoolArticle.HEADER_POST)
