# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class SchoolEvaluation(http.Controller):

    HEADER_GET = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    
    @http.route('/api/parent/evaluations', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluations(self, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        student_ids = partner_id.student_ids
        print "#### Parent : ",partner_id.name
        list_of_evaluation = []
        for student in student_ids:
            print "#### student name ", student.name
            print "#### student last_name", student.last_name
            evaulations = request.env['school.evaluation'].search(['&',('student_id','=',student.id),('state','=','confirmed')])
            eval_list = []
            for evaluation in evaulations:
                domain = [
                    ('res_model', '=', evaluation._name),
                    ('res_field', '=', 'file'),
                    ('res_id', '=', evaluation.id),
                ]
                attachment = request.env['ir.attachment'].search(domain)
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+evaluation.file_name
                url = url.replace(" ", "_")
                _logger.warning('#### URL of document %s',url)
                eval_list.append({
                    'id':evaluation.id,
                    'title':evaluation.name,
                    'date':evaluation.date,
                    'file':evaluation.file,
                    'filename':evaluation.file_name,
                    'url':url,
                    'class':{
                        'id':evaluation.class_id.id,
                        'name':evaluation.class_id.name,
                        'code':evaluation.class_id.code,
                    }
                })

            list_of_evaluation.append({
                'id':student.id,
                'name':student.name,
                'last_name':student.last_name,
                'evaluations':eval_list,
            })
        response['success'] = True
        response['data'] = list_of_evaluation
        return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET)


    @http.route('/api/parent/student/<int:student_id>/evaluations', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluations_by_student(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        list_of_evaluation = []
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER)  

        print "#### student name ", student_id.name
        print "#### student last_name", student_id.last_name
        evaulations = request.env['school.evaluation'].search(['&',('student_id','=',student_id.id),('state','=','confirmed')])
        eval_list = []
        for evaluation in evaulations:
            domain = [
                ('res_model', '=', evaluation._name),
                ('res_field', '=', 'file'),
                ('res_id', '=', evaluation.id),
            ]
            attachment = request.env['ir.attachment'].search(domain)
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+evaluation.file_name
            url = url.replace(" ", "_")
            _logger.warning('#### URL of document %s',url)
            eval_list.append({
                'id':evaluation.id,
                'title':evaluation.name,
                'date':evaluation.date,
                'file':evaluation.file,
                'filename':evaluation.file_name,
                'url':url,
                'class':{
                    'id':evaluation.class_id.id,
                    'name':evaluation.class_id.name,
                    'code':evaluation.class_id.code,
                }
            })
        response['success'] = True
        response['data'] = eval_list
        return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET)


    @http.route('/api/note/student/<int:student_id>/notes', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evals_by_student(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET)  

        print "######## eval student name ", student_id.name
        print "######## eval student last_name", student_id.last_name
        

        eval_line_ids = request.env['school.evaluation'].search([('class_id','=',student_id.class_id.id),('state','=','confirmed')])
        _logger.warning('####### eval IDS :  %s', eval_line_ids)

        eval_list = []
        for eval in eval_line_ids:
            eval_json = {
                'id':eval.id,                
                'academic_year': {
                    'id':eval.academic_year.id,
                    'name':eval.academic_year.name,
                    'code':eval.academic_year.code,
                },
               #'session': {
                   # 'name':eval.timing_system_id.name,
                   # 'timinig_system':eval.timing_system_id, 
                    
                #},
                #'periode': {
                  #'timing_periode':eval.timing_periode_ids,
                  #'name':eval.timing_periode_ids.name,
                #},
                'grade':{
                    'id':eval.grade_id.id,
                    'name':eval.grade_id.name,
                    'code':eval.grade_id.code,
                },
                'classe':{
                    'id':eval.class_id.id,
                    'name':eval.class_id.name,
                    'code':eval.class_id.code,
                },                
                'subject':{
                    'id':eval.subject_id.id,
                    'name':eval.subject_id.name,
                    'code':eval.subject_id.code,
                },
                'teacher':{
                    'id':eval.teacher_id.id,
                    'name':eval.teacher_id.name,
                    'last_name':eval.teacher_id.last_name,
                },
               
               
                'date': eval.date,
                
                'eval_note': self._load_eval_note_as_json(eval, 'student', student_id),
            }
            eval_list.append(eval_json)
        response['data'] = eval_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET) 


    def _load_eval_note_as_json(self, eval, user_type, user_id):
        note_line = []
        if user_type == 'teacher':
            for line in eval.evaluation_note_line_ids:
                note_line.append({
                    'id':line.id, 
                        #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'note': line.note,
                        # 'comment': line.comment, 
                    })
        if user_type == 'student':
            for line in eval.evaluation_note_line_ids:
                if line.student_id.id == user_id.id:
                    note_line.append({
                    'id':line.id, 
                        #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'note': line.note,
                    'comment': line.comment, 
                    })

        return note_line
           