# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime



class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    fees_id             = fields.Many2many('school.fees', 'school_invoice_fees_rel', 'account_invoice_id', 'school_fees_id')

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        result = super(AccountInvoice, self)._onchange_partner_id()
        product_ids = []
        if self.partner_id.is_school_parent:
            for student in self.partner_id.student_ids:
                for rec in self.env['school.fees'].search([('grade_id','=',student.grade_id.id)]):
                    for product in rec.obligatory_fees_ids:
                        product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
                        product_ids.append((0,0, {'student_id':student.id,'product_id':product_product.id, 'quantity':1}))
                    for product in rec.optional_fees_ids:
                        product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
                        product_ids.append((0,0, {'student_id':student.id, 'product_id':product_product.id, 'quantity':1}))
                        
        result['value'] = {'invoice_line_ids':product_ids}
        return  result

    @api.onchange('invoice_line_ids')
    def _change_invoice_lines(self):
        ''' 
            dummy method to trigger _onchange_product_id() of each invoice_line
            in order to calculate the unit_price and subtotals ... 
        '''
        for el in self.invoice_line_ids:
           el._onchange_product_id()

class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    student_id          = fields.Many2one('school.student', string="Élève")



