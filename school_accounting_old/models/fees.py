# -*- coding: utf-8 -*-


from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime


class Fees(models.Model):
    _name = 'school.fees'
    _description = 'Registration fees'


    @api.depends('grade_id','academic_year_id')
    @api.multi
    def _compute_name(self):
        for record in self:
            if record.grade_id and record.academic_year_id:
                record.name = record.grade_id.code+' ['+record.academic_year_id.code+']'

    @api.depends('optional_fees_ids','obligatory_fees_ids')
    @api.multi
    def _compute_fees_amount(self):
        for record in self:
            total_obligatory_fees = 0.0
            total_optional_fees = 0.0
            
            for fees in record.obligatory_fees_ids:
                total_obligatory_fees+=fees.amount
            record.total_obligatory_fees = total_obligatory_fees

            for fees in record.optional_fees_ids:
                total_optional_fees+=fees.amount
            record.total_optional_fees = total_optional_fees

            record.total_amount = total_obligatory_fees + total_optional_fees


    name                    = fields.Char('Name', compute=_compute_name, readonly=True, store=True)
    grade_id                = fields.Many2one('school.grade','Grade',help='Niveau scolaire', required=True)
    obligatory_fees_ids     = fields.Many2many('product.template', 'fees_obligatory_product_template_rel', 
                                               'fees_id', 'product_template_id', string='Obligatory Fees',
                                                domain="['&',('obligatory','=',True), ('is_school_fees','=',True)]")
    optional_fees_ids       = fields.Many2many('product.template', 'fees_optional_product_template_rel',
                                                'fees_id', 'product_template_id',string='Optional Fees',
                                                domain="['&',('obligatory','=',False), ('is_school_fees','=',True)]")



    total_obligatory_fees   = fields.Float(string="Total Obligatory Fees")
    total_optional_fees     = fields.Float(string="Total Optional Fees")
    total_amount            = fields.Float(string="Total Fees")
    academic_year_id        = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), required=True)


    _sql_constraints = [('code_uniq', 'unique(grade_id,academic_year_id)','You already created a Fees Structure for this grade with the same year')]


''' 
    @To-Do 
    inherit from product.product because account.invoice.line is using the product.product class
    and the product.template may generate conflicts because many product.product can be related 
    to one product.template

    update :meth _onchange_partner_id() access the product.product directly without product_templ_id
    because right now the search can return a recordset with multi values
 '''
class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_school_fees      = fields.Boolean('Is School Fees')
    obligatory          = fields.Boolean('Obligatory Fees',default=False)
    academic_year_id    = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), required=True)
    #fees_id             = fields.Many2many('school.fees')

                
# class AccountInvoice(models.Model):
#     _inherit = 'account.invoice'
    
#     fees_id             = fields.Many2many('school.fees', 'school_invoice_fees_rel', 'account_invoice_id', 'school_fees_id')

#     @api.onchange('partner_id', 'company_id')
#     def _onchange_partner_id(self):
#         result = super(AccountInvoice, self)._onchange_partner_id()
#         product_ids = []
#         if self.partner_id.is_school_parent:
#             for student in self.partner_id.student_ids:
#                 for rec in self.env['school.fees'].search([('grade_id','=',student.grade_id.id)]):
#                     for product in rec.obligatory_fees_ids:
#                         product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
#                         product_ids.append((0,0, {'student_id':student.id,'product_id':product_product.id, 'quantity':1}))
#                     for product in rec.optional_fees_ids:
#                         product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
#                         product_ids.append((0,0, {'student_id':student.id, 'product_id':product_product.id, 'quantity':1}))
                        
#         result['value'] = {'invoice_line_ids':product_ids}
#         return  result

#     @api.onchange('invoice_line_ids')
#     def _change_invoice_lines(self):
#         ''' 
#             dummy method to trigger _onchange_product_id() of each invoice_line
#             in order to calculate the unit_price and subtotals ... 
#         '''
#         for el in self.invoice_line_ids:
#            el._onchange_product_id()

# class AccountInvoiceLine(models.Model):
#     _inherit = "account.invoice.line"

#     student_id          = fields.Many2one('school.student', string="Élève")
