{
    'name': "Comptabilité de l'école",
    'version': '1.0',
    'category': 'Scolarité',
    'author': 'Ben Selma Sghaier',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school','subscription'],
    'data': [
        'security/ir.model.access.csv',
        'views/account_invoice_view.xml',
        'views/fees_view.xml',
        'views/cron_view.xml',
        'views/payement_view.xml',

        'views/report_template_layout.xml',
        'views/report.xml',
        'views/report_template.xml',
        'views/report_canteen.xml',
        'views/receipt_payment.xml',

    ],
    #'qweb': ['static/src/xml/*.xml'],

    'installable': True,
    'auto_install': False,
    'application': True,
}