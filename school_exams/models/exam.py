# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)

class Config(models.Model):
    _name  = 'evaluation.periode.config'

    annual_calendar_id      = fields.Many2one('annual.calendar', string="Année scolaire", default=lambda self: self.env['annual.calendar'].get_current_year())
    module_id               = fields.Many2one('module.module', string="Module")
    skill_id                = fields.Many2one('module.skill', domain="[('module_id','=',module_id)]", string="Compétence")
    
    annual_periode_id       = fields.Many2one('annual.periode', string="Semestre")
    periode_id              = fields.Many2one('annual.periode', string="Période")
    educational_stage_id    = fields.Many2one('educational.stage', string="Cycle")
    evaluated               = fields.Boolean('évalué', default=True)

    _sql_constraints = [ ('name_uniq', 'unique(module_id,annual_periode_id,educational_stage_id)', 'Erreur de duplication de données'),]

    @api.onchange('educational_stage_id')
    def _onchang_module_id(self):
        module_id = self.env['module.module'].get_modules_contains_cycle(self.educational_stage_id.id)
        _logger.warning('#### FOUND MODULES %s', module_id)
        return {
                    'domain':{
                        'module_id':[('id','in',module_id.ids)],
                    }
                }


class Exam(models.Model):
    _name   = 'exam.exam'

    name                    = fields.Char('Nom')


# class Exam(models.Model):
#     _name = 'exam.planning'
#     _description = 'Planning of exams'


#     name                    = fields.Char('Nom')
#     grade_id                = fields.Many2one('school.grade',  string='Niveau scolaire', required=True)
#     class_id                = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", string="Classe", required=True)
#     exam_type_id            = fields.Many2one('school.exam.type', string="Type d'examen", required=True)

#     annual_calendar_id      = fields.Many2one('annual.calendar', string='Années scolaire', required=True)
#     periode_id              = fields.Many2one('annual.periode', string="Semestre", required=True, 
#                               domain="['&',('annual_calendar_id','=',annual_calendar_id),('last_level','=',False)]")
#     step_id                 = fields.Many2one('annual.periode', string="étape", required=True, 
#                                domain="['&',('parent_id','=',periode_id),('last_level','=',True)]")
#     ponderation             = fields.Integer('Pondération du module', required=True)
#     exam_module_ids         = fields.One2many('exam.module', 'exam_planning_id')


#     _sql_constraints = [ ('name_uniq', 'unique(class_id,exam_type_id,annual_calendar_id,periode_id,step_id)', 'Erreur de duplication de données'),]

#     @api.multi
#     def name_get(self):
#         return [(record.id, record.exam_type_id.name+' '+record.periode_id.name )for record in self]


#     @api.one
#     @api.constrains('ponderation')
#     def _check_ponderation(self):
#         if self.ponderation <= 0 or self.ponderation > 100:
#             raise ValidationError("La valeur de pondération doit être entre 0 et 100")

# class ExamModule(models.Model):
#     _name = 'exam.module'

#     name                    = fields.Char('Nom')
#     exam_planning_id        = fields.Many2one('exam.planning', ondelete='cascade')
#     module_id               = fields.Many2one('school.module', string="Module", required=True)
#     exam_exam_ids           = fields.One2many('exam.exam', 'exam_module_id', string="Examens")
#     exam_module_note_id     = fields.One2many('exam.module.note', compute='_compute_module_note')
#     not_evaluated           = fields.Boolean('Non évalué', default=False, required=True)


#     _sql_constraints = [ ('name_uniq', 'unique(exam_planning_id,module_id)', 'Erreur de duplication de données'),]



#     @api.multi
#     def _compute_module_note(self):
#         for rec in self:
#             exam_module_note_id = self.env['exam.module.note'].search([('exam_module_id','=',rec.id)])
#             for exam_line in exam_module_note_id.exam_line_ids:
#                 for line in exam_line:
#                     print "####### name of exam ",line.name


#     @api.model
#     def create(self, vals):
#         res = super(ExamModule, self).create(vals)
#         ponderation = 0
#         for rec in res.exam_exam_ids:
#             ponderation+= rec.ponderation
#         print "#### SOMME ",ponderation
#         if ponderation > 100:
#             raise ValidationError("La somme des pondérations des examens ne doit pas dépasser 100 module [%s]"%(self.name))
#         return res

#     @api.multi
#     def write(self, vals):
#         res = super(ExamModule, self).write(vals)
#         for rec in self:
#             ponderation = 0
#             for exam in rec.exam_exam_ids:
#                 ponderation+= exam.ponderation
#             print "#### SOMME ",ponderation
#             if ponderation > 100:
#                 raise ValidationError("La somme des pondérations des examens ne doit pas dépasser 100 module [%s]"%(rec.name))

#         return res



# class ExamExam(models.Model):
#     _name = 'exam.exam'

#     name                    = fields.Char('Nom')
#     exam_module_id          = fields.Many2one('exam.module', ondelete='cascade')
#     subject_id              = fields.Many2one('school.subject', string="Matière", required=True)
#     skill_id                = fields.Many2one('school.subject.skill', string="Compétence")
#     teacher_id              = fields.Many2one('hr.employee', string='Enseignant', required=True)
#     note_max                = fields.Integer(default=0.0, string="Note max", required=True)
#     start_date              = fields.Datetime('Date début', default=fields.Datetime.now(), required=True)
#     end_date                = fields.Datetime('Date fin', default=fields.Datetime.now(), required=True)
#     ponderation             = fields.Integer(string="Pondération")

#     _sql_constraints = [ ('name_uniq', 'unique(exam_module_id,subject_id,)', 'Erreur de duplication de données'),]


#     @api.onchange('exam_module_id')
#     def _onchange_exam_module_id(self):
#         return {'domain':{'subject_id':[('id','in',self.exam_module_id.module_id.subject_ids.ids)]}}

#     @api.onchange('subject_id')
#     def _onchange_subject_id(self):
#         return {
#                     'domain':{
#                         'skill_id':[('id','in',self.subject_id.skill_ids.ids)],
#                         'teacher_id':[('id','in',self.subject_id.teacher_ids.ids)]
#                     }
#                 }

#     @api.one
#     @api.constrains('ponderation')
#     def _check_ponderation(self):
#         if self.ponderation <= 0 or self.ponderation > 100:
#             raise ValidationError("La valeur de pondération doit être entre 0 et 100 module [%s] examen [%s]"%(self.exam_module_id.name,self.subject_id.name ))


#     @api.one
#     @api.constrains('note_max')
#     def _check_note_max(self):
#         if self.note_max <= 0:
#             raise ValidationError("La note maximale doit être strictement supérieure à 0 module [%s] examen [%s]"%(self.exam_module_id.name,self.subject_id.name ))


#     @api.one
#     @api.constrains('start_date', 'end_date')
#     def _check_dates(self):
#         if self.start_date >= self.end_date:
#             raise ValidationError("La date de fin d'examen doit être supérieure à la date de début module [%s] examen [%s]"%(self.exam_module_id.name,self.subject_id.name ))



# class Module(models.Model):
#     _name = 'module.module'

#     name                            = fields.Char('Nom', required=True)
#     educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
#     skill_ids                       = fields.One2many('module.skill', 'module_id', string="Cométences")
#     ponderation                     = fields.Integer('Pondération', default=0)


#     @api.model
#     def create(self, vals):
#         res = super(Module, self).create(vals)
#         ponderation = 0
#         for rec in res.skill_ids:
#             ponderation+= rec.ponderation
#         print "#### SOMME ",ponderation
#         if ponderation > 100:
#             raise ValidationError("La somme des pondérations des compétences ne doit pas dépasser 100 module [%s]"%(self.name))
#         return res

#     @api.multi
#     def write(self, vals):
#         res = super(Module, self).write(vals)
#         for rec in self:
#             ponderation = 0
#             for skill in rec.skill_ids:
#                 ponderation+= skill.ponderation
#             print "#### SOMME ",ponderation
#             if ponderation > 100:
#                 raise ValidationError("La somme des pondérations des compétences ne doit pas dépasser 100 module [%s]"%(rec.name))
#         return res

# class Skill(models.Model):
#     _name = 'module.skill'


#     name                = fields.Char('Nom', required=True)
#     ponderation         = fields.Integer('Pondération', default=0, required=True)
#     module_id           = fields.Many2one('module.module', string="Module", ondelete="cascade")
#     note_max            = fields.Integer('Note maximale', required=True)

