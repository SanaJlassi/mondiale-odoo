# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class ExamPeriodeConfig(models.Model):
    _name = 'school.exam.periode.config'


    name                            = fields.Char('Nom', required=True)
    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    periode_ids                     = fields.One2many('school.exam.periode','periode_config_id', string="Périodes")
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)

    @api.multi
    def name_get(self):
        return [(record.id, record.academic_year.code+'/'+record.name )for record in self]


class ExamPeriode(models.Model):
    _name = 'school.exam.periode'

    name                    = fields.Char('Nom du période', required=True)
    code                    = fields.Char('Code du période', required=True)
    periode_config_id       = fields.Many2one('school.exam.periode.config')
    academic_year           = fields.Many2one(related='periode_config_id.academic_year')
    educational_stage_id    = fields.Many2one(related='periode_config_id.educational_stage_id')
    start_date              = fields.Date('Date début', required=True)
    end_date                = fields.Date('Date fin', required=True)
    exam_type_ids           = fields.Many2many('school.exam.type', string="Type examen", domain="[('educational_stage_id','=',educational_stage_id)]")



    # @api.returns('self')
    # @api.multi
    # def get_periode_by_cycle(self, cycle_id):
    #     periode_id = self.env['annual.periode'].search([])
    #     return module_id.filtered(lambda r: cycle_id in r.educational_stage_ids.ids)



    @api.one
    @api.constrains('start_date', 'end_date')
    def _check_dates(self):
        if self.start_date >= self.end_date:
            raise ValidationError("La date de fin de période doit être supérieure à la date de début de période")
        

    # @api.model
    # def create(self, vals):
    #     res = super(AnnualPeriode, self).create(vals)
    #     start_date = datetime.strptime(res.start_date, DEFAULT_SERVER_DATE_FORMAT)
    #     end_date = datetime.strptime(res.end_date, DEFAULT_SERVER_DATE_FORMAT)
    #     if res.annual_calendar_id.start_date and res.annual_calendar_id.end_date:
    #         year_start = datetime.strptime(res.annual_calendar_id.start_date, DEFAULT_SERVER_DATE_FORMAT)
    #         year_end = datetime.strptime(res.annual_calendar_id.end_date, DEFAULT_SERVER_DATE_FORMAT)

    #         if  start_date < year_start or start_date > year_end:
    #             raise ValidationError("Date de début de période [%s] doit être entre l'intervalle d'année "%res.name)
    #         if  end_date < year_start or end_date > year_end:
    #             raise ValidationError("Date de fin de période [%s] doit être entre l'intervalle d'année "%res.name)

    #     if res.parent_id:
    #         parent_start = datetime.strptime(res.parent_id.start_date, DEFAULT_SERVER_DATE_FORMAT)
    #         parent_end = datetime.strptime(res.parent_id.end_date, DEFAULT_SERVER_DATE_FORMAT)
    #         if  start_date < parent_start or start_date > parent_end:
    #             raise ValidationError("Date de début de période [%s] doit être entre l'interval du niveau superieur"%res.name)
    #         if  end_date < parent_start or end_date > parent_end:
    #             raise ValidationError("Date de fin de période [%s] doit être entre l'interval du niveau superieur"%res.name)

        
    #     return res
