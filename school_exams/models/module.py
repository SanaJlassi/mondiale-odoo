# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)

class Marks(models.Model):
    _name = 'module.mark'

    
    name                            = fields.Char('Nom', required=True)
    description                     = fields.Char('Description', required=True)



class Module(models.Model):
    _name = 'module.module'

    order_value                     = fields.Integer(default=0)
    name                            = fields.Char('Nom', required=True)
    educational_stage_ids           = fields.Many2many('educational.stage', string="Cycles", required=True)
    skill_ids                       = fields.One2many('module.skill', 'module_id', string="Cométences")
    ponderation                     = fields.Integer('Pondération', default=0)
    evaluated               		= fields.Boolean('A évaluer', default=False)
    module_mark_id                  = fields.Many2many('module.mark', string="Remarques")

    @api.returns('self')
    @api.multi
    def get_modules_contains_cycle(self, cycle_id):
    	module_id = self.env['module.module'].search([])
    	return module_id.filtered(lambda r: cycle_id in r.educational_stage_ids.ids)


    @api.model
    def create(self, vals):
        res = super(Module, self).create(vals)
        # ponderation = 0
        # for rec in res.skill_ids:
        #     ponderation+= rec.ponderation
        # print "#### SOMME ",ponderation
        # if ponderation > 100:
        #     raise ValidationError("La somme des pondérations des compétences ne doit pas dépasser 100 module [%s]"%(self.name))
        return res

    @api.multi
    def write(self, vals):
        res = super(Module, self).write(vals)
        # for rec in self:
        #     ponderation = 0
        #     for skill in rec.skill_ids:
        #         ponderation+= skill.ponderation
        #     print "#### SOMME ",ponderation
        #     if ponderation > 100:
        #         raise ValidationError("La somme des pondérations des compétences ne doit pas dépasser 100 module [%s]"%(rec.name))
        return res


class Skill(models.Model):
    _name = 'module.skill'

    name                = fields.Char('Nom', required=True)
    ponderation         = fields.Integer('Pondération', default=0, required=True)
    module_id           = fields.Many2one('module.module', string="Module", ondelete="cascade")
    note_max            = fields.Integer('Note maximale', required=True)
    is_skill            = fields.Boolean('Une compétence ?', default=True)
    to_skip             = fields.Boolean('Ignore', default=False)
    fake_ponderation   = fields.Integer('Fake ponderation', default=0,)

