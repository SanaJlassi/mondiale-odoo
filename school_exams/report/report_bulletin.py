# -*- coding: utf-8 -*-
from datetime import datetime
import time
from odoo import api, models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)


class ReportBulletin(models.AbstractModel):
    _name = 'report.school_exams.report_studentbulletin'




    def _get_module_val(self, education_stage):
        grade_id = education_stage.grade_ids
        grade_list = []
        for grade in grade_id:
            grade_list.append({
                'name':grade.name,
                
                })


    def _get_semester_val(self,periode_config):
        if periode_config.educational_stage_id.evaluation_methode == 'preschool':
            semester_id = periode_config.periode_ids.filtered(lambda r: r.last_level == True)
        else:
            semester_id = periode_config.periode_ids.filtered(lambda r: r.last_level == False)
        semester_list = []
        for semester in semester_id:
            semester_list.append({
                'id':semester.id,
                'name':semester.name,
                'code':semester.code,
                'start_date':semester.start_date,
                'end_date':semester.end_date,
                'steps':self._get_step_val(semester),
                # 'class_days':semester.class_days,
            })
        # _logger.warning('##### semester_list %s', semester_list)
        return semester_list

    def _get_step_val(self,semester):
        step_id = semester.child_id
        step_list = []
        for step in step_id:
            step_list.append({
                'id':step.id,
                'name':step.name,
                'code':step.code,
                'order_value':step.order_value,
                'start_date':step.start_date,
                'end_date':step.end_date,
                'evaluated':step.evaluated,
                'last_level':step.last_level,
                'ponderation': step.ponderation ,
            })
        return step_list

    def is_last(self, current_periode, last_periode, current_grade, grade):
        if current_periode['order_value'] <= last_periode.order_value and current_grade.id == grade.id:
            return True
        else:
            return False






    def _get_periode_config(self, education_stage, annual_calendar):
        # domain = [('educational_stage_id','=',education_stage),('annual_calendar_id','=',annual_calendar)]
        # periode_config_id = self.env['periode.config'].search(domain,limit=1)
        # _logger.warning('##### periode_config_id %s', periode_config_id)
        # return periode_config_id if len(periode_config_id) else False
        # _logger.warning('##### education_stage system %s', education_stage.id)
        # _logger.warning('##### annual_calendar system %s', annual_calendar)
        periode_config_id = self.env['periode.config'].search(['&',('annual_calendar_id','=',annual_calendar.id),('educational_stage_id','=',education_stage.id)])
        # _logger.warning('##### periode_config_id %s', periode_config_id)
        return periode_config_id



    # def _get_semester(self, periode_config):
    #     return periode_config_id.periode_ids.filtered(lambda r: r.last_level == False)

    def _get_steps(self, periode):
        return periode.child_id.filtered(lambda r: r.last_level == True)


    def _get_modules(self, education_stage):
        module_list = self.env['module.module']
        module_id = self.env['module.module'].search([])
        # _logger.warning('##### education stage id %s', education_stage.id)
        for module in module_id:
            # _logger.warning('##### MODULE ID %s', module)
            for ed_stage in module.educational_stage_ids:
                # _logger.warning('##### ed_stage id %s', ed_stage.id)
                if education_stage.id == ed_stage.id:
                    module_list+=module
                    # _logger.warning('##### module list %s', module_list)
        #return module_list.sorted(key=lambda r: r.order_value)        
        return module_list.sorted(key=lambda r: r.order_value)



    def _get_skills(self, module):
        return module.skill_ids


    def _get_note(self, module, skill, grade, class_id, semester, step, o):
        note_list = []
        note_list = self.env['class.note'].search(['&',('module_id','=',module.id),'&',('skill_id','=',skill.id),'&',('grade_id','=',grade.id),'&',('class_id','=',class_id.id),'&',('semestre_id.code','=',semester['code']),('periode_id.code','=',step['code'])])
        if len(note_list):
            note = note_list.mapped('note_line_ids').filtered(lambda r: r.student_id.id == o.id)
            return int(note.note * (100/float(note_list.skill_id.note_max)))
        else:
            return False

    def _get_attendance(self,grade_id, class_id, semester_id,  o):
        attendance_list = []
        attendance_list = self.env['school.attendance'].search([('class_id.grade_id','=',grade_id.id),('class_id','=',class_id.id),('periode_id','=',semester_id['id'])])
        print "###### ATTENDANCE STUDENT",attendance_list
        att = 0
        if len(attendance_list):
            attendance_lines = attendance_list.mapped('attendance_line_ids').filtered(lambda r: r.student_id.id == o.id)            
            for line in attendance_lines:
                print "###### ATTENDANCE STUDENT STATE",line.state
                if line.state == 'absent':
                    att += 1
            return att 
        else:
            return '-'
            # return False


    def _get_teacher(self, module, grade, class_id, semester, step, o):
        note_list = []
        note_list = self.env['class.note'].search([('module_id','=',module.id),('grade_id','=',grade.id),('class_id','=',class_id.id),('semestre_id','=',semester.id),('periode_id','=',step.id)])
        if len(note_list):
            teacher_id = note_list[0].teache_id
            teacher_name = teacher_id.name +' '+ teacher_id.last_name
            return teacher_name
        else:
            return '-'



    def _get_comment(self, module, grade, class_id, semester, step, o):
        comment_list = []
        domain =  [('module','=',module.id),('grade_id','=',grade.id),('class_id','=',class_id.id),('semestre_id','=',semester.id),('periode_id','=',step.id)]
        # print "###### DOMAIN ",domain
        comment_list = self.env['teacher.comment'].search(domain)
        # print "######### COMMENT  ", comment_list
        if len(comment_list):
            tmp_comment = comment_list.mapped('teacher_comment_line_ids').filtered(lambda r: r.student_id.id == o.id)
            if len(tmp_comment):
                comment = tmp_comment[0]
                # print "######### COMMENT  ",comment.comment
                return comment.comment
        else:
            return '-'

    def _get_comment_presco(self, grade, class_id, semester, o):
        comment_list = []
        domain =  [('grade_id','=',grade.id),('class_id','=',class_id.id),('semestre_id','=',semester.id)]
        # print "###### DOMAIN ",domain
        comment_list = self.env['admin.comment'].search(domain)
        # print "######### COMMENT  ", comment_list
        if len(comment_list):
            tmp_comment = comment_list.mapped('admin_comment_line_ids').filtered(lambda r: r.student_id.id == o.id)
            if len(tmp_comment):
                comment = tmp_comment[0]
                # print "######### COMMENT  ",comment.comment
                return comment.comment
        else:
            return '-'

    def _get_note_presco(self, module,skill, grade, class_id, semester,o):
        note_list = []
        note_list = self.env['class.note'].search(['&',('module_id','=',module.id),'&',('skill_id','=',skill.id),'&',('grade_id','=',grade.id),'&',('class_id','=',class_id.id),('semestre_id.code','=',semester['code'])])
        for rec in note_list:
            for line in rec.presconote_line_ids:               
                if line.student_id.id == o.id:
                    note = line.mark.name
            return note


    def get_module_average(self, module, grade,  semester, step, annual_calendar, student):
        domain = [('class_id','=',student.class_id.id),('module_id','=',module.id),('grade_id','=',grade.id),('annual_calendar_id','=',annual_calendar.id),('periode_id','=',step['id']),('semestre_id','=',semester['id'])]
        note_id = self.env['class.note'].search(domain)
        if len(note_id):
            skills = note_id.mapped('skill_id')
            average_class_note = 0
            skill_with_no_note = module.skill_ids.filtered(lambda r: r not in skills)
            distributed_ponderation = sum(skill_with_no_note.mapped('ponderation'))/len(skills)

            for skill in skills:
                notes = note_id.filtered(lambda r: r.skill_id.id == skill.id)
                class_note = notes.mapped('note_line_ids.note')
                if not skill.is_skill:
                    average_class_note += sum(note * (100/float(skill.note_max))  for note in class_note)
                else:
                    average_class_note += sum(note * (float(skill.ponderation+distributed_ponderation)/100) * (100/float(skill.note_max))  for note in class_note)
            return int(average_class_note/len(student.class_id.student_ids))
        else:
            return False

    def get_student_module_average(self, module, grade, semester, step, annual_calendar, student):
        
        domain = [('class_id','=',student.class_id.id),('module_id','=',module.id),('grade_id','=',grade.id),('annual_calendar_id','=',annual_calendar.id),('periode_id','=',step['id']),('semestre_id','=',semester['id'])]
        note_id = self.env['class.note'].search(domain)
        if len(note_id):
            skills = note_id.mapped('skill_id')
            average_class_note = 0
            skill_with_no_note = module.skill_ids.filtered(lambda r: r not in skills)
            distributed_ponderation = sum(skill_with_no_note.mapped('ponderation'))/len(skills)

            for skill in skills:
                notes = note_id.filtered(lambda r: r.skill_id.id == skill.id)
                class_note = notes.mapped('note_line_ids.note')
                student_note = notes.mapped('note_line_ids').filtered(lambda r: r.student_id.id == student.id)
                if not skill.is_skill:
                    average_class_note += student_note.note * (100/float(skill.note_max)) 
                else:
                    average_class_note += student_note.note * (float(skill.ponderation+distributed_ponderation)/100) * (100/float(skill.note_max)) 
            return int(average_class_note)
        else:
            return False

    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        print "##### render_html"
        grade_id            = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        class_id            = 'class_id' in data['form'] and data['form']['class_id'] or False
        annual_calendar_id  = 'annual_calendar_id' in data['form'] and data['form']['annual_calendar_id'] or False
        periode_id          = 'periode_id' in data['form'] and data['form']['periode_id'] or False
        step_id             = 'step_id' in data['form'] and data['form']['step_id'] or False
        student_id          = 'student_id' in data['form'] and data['form']['student_id'] or False


        student_id                = self.env['school.student'].search([('id','in',student_id)])
        print "###### STUDENTS ",student_id
        grade_id                = self.env['school.grade'].search([('id','=',grade_id[0])])
        educational_stage_id    = grade_id.educational_stage_id
        class_id                = self.env['school.class'].search([('id','=',class_id[0])])
        annual_calendar_id      = self.env['annual.calendar'].search([('id','=',annual_calendar_id[0])])
        semester_id             = self.env['annual.periode'].search([('id','=',periode_id[0])])
        print "###### step_id",step_id
        if step_id != False:
            step_id                 = self.env['annual.periode'].search([('id','=',step_id[0])])




        # student_id              = class_id.student_ids

       
        print "##### render_html 2"
        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['res.partner'],
            'data': data,
            'docs': student_id,
            'time': time,
            'grade_id':grade_id,
            'educational_stage_id': educational_stage_id,
            'step_id':step_id,
            'semester_id':semester_id,
            'class_id':class_id,
            'steps':self._get_steps,
            'modules':self._get_modules,
            'skills':self._get_skills,
            'get_teacher':self._get_teacher,

            'annual_calendar_id': annual_calendar_id,
            'periode_config':self._get_periode_config,
            'semesters':self._get_semester_val,
            'notes':self._get_note,
            'average':self.get_module_average,
            'student_average':self.get_student_module_average,
            'notes_presco':self._get_note_presco,
            'is_last':self.is_last,
            'comments':self._get_comment,
            'attendances':self._get_attendance,
            'comments_presco':self._get_comment_presco,
        }
        # _logger.warning('##### DOCARGS %s', docargs)
        report = self.env['report'].render('school_exams.report_studentbulletin', docargs)
        print "-------------------------- REPORT ",type(report)
        # print "-------------------------- REPORT ",report
        print "---------------------------------------------------------------------------------------"
        return report
