# -*- coding: utf-8 -*-
from datetime import datetime
import time
from odoo import api, models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)


class ExamTranscript(models.AbstractModel):
    _name = 'report.school_exams.report_schoolexamtranscript'

    def _get_teachers(self, class_id):
        teacher_list = []
        teacher_list = self.env['hr.employee'].search([('class_ids','=',class_id)])
        print "################### TEACHER LIST",teacher_list
        return teacher_list

    def _get_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id, student_id):
        notes_list = []
        # print "################ GET NOTE 1"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
                average = note_id.average
            return average
        else:
            return '-'

    def _get_max_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
        notes_list = []
        print "################ GET NOTE 2"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids')
                average = note_id.mapped('average')
            return max(average)
        else:
            return '-'

    def _get_min_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
        notes_list = []
        print "################ GET NOTE 3"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids')
                average = note_id.mapped('average')
            return min(average)
        else:
            return '-'


    def _get_appreciation_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id):
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('module_id','=',module_id.id)] 
        module_list = self.env['exam.appreciation.module'].search(domain)
        if len(module_list):
            appreciation_id = module_list.mapped('comment_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return appreciation_id.comment
        else:
            return '-'

    def _get_average_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id):
        notes_list = []
        # print "################ GET NOTE 1"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('module_id','=',module_id.id)]
        # print "################ DOMAIN",domain
        notes_list = self.env['exam.appreciation.module'].search(domain)
        # print "################ NOTES LIST",notes_list
        if len(notes_list):
            note_id = notes_list.mapped('module_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            # print "###################### NOTE",note_id
            return note_id.average
        else:
            return '-'



    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        print "##### render_html"
        academic_year                 = 'academic_year' in data['form'] and data['form']['academic_year'] or False
        educational_stage_id          = 'educational_stage_id' in data['form'] and data['form']['educational_stage_id'] or False
        periode_config_id             = 'periode_config_id' in data['form'] and data['form']['periode_config_id'] or False
        periode_ids                   = 'periode_ids' in data['form'] and data['form']['periode_ids'] or False
        grade_id                      = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        class_id                      = 'class_id' in data['form'] and data['form']['class_id'] or False
        student_id                    = 'student_id' in data['form'] and data['form']['student_id'] or False


        student_id                    = self.env['school.student'].search([('id','in',student_id)])
        grade_id                      = self.env['school.grade'].search([('id','=',grade_id[0])])
        educational_stage_id          = self.env['educational.stage'].search([('id','=',educational_stage_id[0])])
        class_id                      = self.env['school.class'].search([('id','=',class_id[0])])
        periode_ids                   = self.env['school.exam.periode'].search([('id','=',periode_ids[0])])
        academic_year                 = self.env['academic.year'].search([('id','=',academic_year[0])])
        periode_config_id             = self.env['school.exam.periode.config'].search([('id','=',periode_config_id[0])])
       
        print "##### render_html 2"
        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'docs': student_id,
            'time': time,
            'academic_year': academic_year,
            'educational_stage_id':educational_stage_id,
            'periode_config_id': periode_config_id,
            'periode_ids': periode_ids,
            'grade_id': grade_id,
            'class_id': class_id,
            'teacher_list': self._get_teachers,
            # 'notes_list': self._get_notes,
            'average_subject': self._get_average_subject,
            'max_average_subject': self._get_max_average_subject,
            'min_average_subject' : self._get_min_average_subject,
            'appreciation_module': self._get_appreciation_module,
            'average_module': self._get_average_module,
        }
        report = self.env['report'].render('school_exams.report_schoolexamtranscript', docargs)
        return report
