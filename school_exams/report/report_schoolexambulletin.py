# -*- coding: utf-8 -*-
from datetime import datetime
import time
from odoo import api, models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)


class ExamBulletin(models.AbstractModel):
    _name = 'report.school_exams.report_schoolexambulletin'

    def _get_teachers(self, class_id):
        teacher_list = []
        teacher_list = self.env['hr.employee'].search([('class_ids','=',class_id)])
        print "################### TEACHER LIST",teacher_list
        return teacher_list

    def _get_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id, student_id):
        notes_list = []
        # print "################ GET NOTE 1"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
                average = note_id.average
            return average
        else:
            return 00

    def _get_max_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
        notes_list = []
        print "################ GET NOTE 2"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids')
                average = note_id.mapped('average')
            return max(average)
        else:
            return '-'

    def _get_min_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
        notes_list = []
        print "################ GET NOTE 3"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids')
                average = note_id.mapped('average')
            return min(average)
        else:
            return '-'


    def _get_appreciation_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id):
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('module_id','=',module_id.id)] 
        module_list = self.env['exam.appreciation.module'].search(domain)
        if len(module_list):
            appreciation_id = module_list.mapped('comment_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return appreciation_id.comment
        else:
            return '-'

    def _get_appreciation_council(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id):
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)] 
        appreciation_list = self.env['exam.appreciation.council'].search(domain)
        if len(appreciation_list):
            appreciation_id = appreciation_list.mapped('comment_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return appreciation_id.comment
        else:
            return '-'

    def _get_average_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('module_id','=',module_id.id)]
        notes_list = self.env['exam.appreciation.module'].search(domain)
        print"#################################################################avrage_module",notes_list
        if len(notes_list):
            note_id = notes_list.mapped('module_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return note_id.average
        else:
            return '-'

    def _get_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
        # print "################ DOMAIN",domain
        notes_list = self.env['exam.appreciation.council'].search(domain)
        if len(notes_list):
            note_id = notes_list.mapped('period_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return note_id.average
        else:
            return '-'

    def _get_max_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
        # print "################ DOMAIN 2 ",domain
        notes_list = self.env['exam.appreciation.council'].search(domain)
        if len(notes_list):
            note_id = notes_list.mapped('period_average_line_ids')
            average = note_id.mapped('average')
            return max(average)
        else:
            return '-'

    def _get_min_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
        # print "################ DOMAIN 2 ",domain
        notes_list = self.env['exam.appreciation.council'].search(domain)
        if len(notes_list):
            note_id = notes_list.mapped('period_average_line_ids')
            average = note_id.mapped('average')
            return min(average)
        else:
            return '-'

    def _get_student_rank(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
        notes_list = self.env['exam.appreciation.council'].search(domain)
        if len(notes_list):
            line = notes_list.mapped('period_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return line.rank
        else:
            return '-'
    def _get_average_french_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,student_id):
        nb_sub=0
        sum_av=0
        sum_average=0
        average_subject=0
        modules=self.env['school.module'].search([("grade_ids","=",grade_id.id)])
        for mod in modules:
            for sub in mod.subject_ids.sorted(key=lambda subject: subject.order_value, reverse=False):
                if sub.is_french == True:
                    if sub.order_value <=5 :
                        sum_average+= self._get_average_subject(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, sub, student_id)
                        print"#####sum_average",sum_average
                    if sub.order_value >=6:
                        nb_sub+=1;
                        print"########nb_sub",nb_sub
                        sum_av+=self._get_average_subject(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,sub,student_id)
                        print"#####sum_av",sum_av
                if nb_sub !=0:       
                    average_subject=sum_av/nb_sub
                print"####average_subject"
                average_french =round((average_subject+sum_average)/4,2)
                print"####average_french", average_french
        return average_french
    def _get_average_english_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,student_id):
        sum_average=0
        modules=self.env['school.module'].search([("grade_ids","=",grade_id.id)])
        for mod in modules:
            for sub in mod.subject_ids.sorted(key=lambda subject: subject.order_value, reverse=False):
                if sub.is_english == True:
                    sum_average+= self._get_average_subject(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, sub, student_id)
                    print"###sum_average",sum_average
                    average_english=round(sum_average/2,2)
                    print"######average_english",average_english
        return average_english

    def _get_average_english_frensh(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,module_id,student_id):
        frensh_average=self._get_average_french_subject(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,student_id)
        english_average=self._get_average_english_subject(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,student_id)
        total_average=round((frensh_average+english_average)/2,2)
        return total_average


    

    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        print "##### render_html"
        academic_year                 = 'academic_year' in data['form'] and data['form']['academic_year'] or False
        educational_stage_id          = 'educational_stage_id' in data['form'] and data['form']['educational_stage_id'] or False
        periode_config_id             = 'periode_config_id' in data['form'] and data['form']['periode_config_id'] or False
        periode_ids                   = 'periode_ids' in data['form'] and data['form']['periode_ids'] or False
        grade_id                      = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        class_id                      = 'class_id' in data['form'] and data['form']['class_id'] or False
        student_id                    = 'student_id' in data['form'] and data['form']['student_id'] or False


        student_id                    = self.env['school.student'].search([('id','in',student_id)])
        grade_id                      = self.env['school.grade'].search([('id','=',grade_id[0])])
        educational_stage_id          = self.env['educational.stage'].search([('id','=',educational_stage_id[0])])
        class_id                      = self.env['school.class'].search([('id','=',class_id[0])])
        periode_ids                   = self.env['school.exam.periode'].search([('id','=',periode_ids[0])])
        academic_year                 = self.env['academic.year'].search([('id','=',academic_year[0])])
        periode_config_id             = self.env['school.exam.periode.config'].search([('id','=',periode_config_id[0])])
       
        print "##### render_html 2"
        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'docs': student_id,
            'time': time,
            'academic_year': academic_year,
            'educational_stage_id':educational_stage_id,
            'periode_config_id': periode_config_id,
            'periode_ids': periode_ids,
            'grade_id': grade_id,
            'class_id': class_id,
            'teacher_list': self._get_teachers,
            'average_subject': self._get_average_subject,
            'max_average_subject': self._get_max_average_subject,
            'min_average_subject' : self._get_min_average_subject,
            'appreciation_module': self._get_appreciation_module,
            'average_module': self._get_average_module,
            'average_period': self._get_average_period,
            'max_average_period': self._get_max_average_period,
            'min_average_period': self._get_min_average_period,
            'appreciation_councils': self._get_appreciation_council,
            'student_rank': self._get_student_rank,
            'average_french_subject':self._get_average_french_subject,
            'average_english_subject':self._get_average_english_subject,
            'total_average':self._get_average_english_frensh,
        }
        report = self.env['report'].render('school_exams.report_schoolexambulletin', docargs)
        print "############# hello"
        return report
