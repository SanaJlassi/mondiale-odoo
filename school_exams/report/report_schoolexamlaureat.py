# -*- coding: utf-8 -*-
from datetime import datetime
import time
from odoo import api, models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)


class ExamLaureat(models.AbstractModel):
    _name = 'report.school_exams.report_schoolexamlaureat'

    def _get_max_student(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
        notes_list = []
        name_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):            
            note_id = notes_list[0].mapped('exam_notes_line_ids')
            newlist = sorted(note_id, key=lambda x: x.average, reverse=True)
            # print "############ NEW LIST",newlist[:3] 
            for i in newlist[:3]:
                name_list.append(i.student_id.name+' '+i.student_id.last_name)   
            print "################################################ name list",name_list        
            return name_list
        else:
            return '-'


    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        print "##### render_html"
        academic_year                 = 'academic_year' in data['form'] and data['form']['academic_year'] or False
        educational_stage_id          = 'educational_stage_id' in data['form'] and data['form']['educational_stage_id'] or False
        periode_config_id             = 'periode_config_id' in data['form'] and data['form']['periode_config_id'] or False
        periode_ids                   = 'periode_ids' in data['form'] and data['form']['periode_ids'] or False
        grade_id                      = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        class_id                      = 'class_id' in data['form'] and data['form']['class_id'] or False
        


        grade_id                      = self.env['school.grade'].search([('id','=',grade_id[0])])
        educational_stage_id          = self.env['educational.stage'].search([('id','=',educational_stage_id[0])])
        class_id                      = self.env['school.class'].search([('id','=',class_id[0])])
        periode_ids                   = self.env['school.exam.periode'].search([('id','=',periode_ids[0])])
        academic_year                 = self.env['academic.year'].search([('id','=',academic_year[0])])
        periode_config_id             = self.env['school.exam.periode.config'].search([('id','=',periode_config_id[0])])

        subject_ids                   = self.env['school.subject'].search([('grade_ids','=',grade_id.id)]) 

        print "##### render_html 2"
        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'docs': subject_ids,
            'time': time,
            'periode_id': periode_ids,
            'grade_id': grade_id,
            'class_id': class_id,
            'academic_year': academic_year,
            'educational_stage_id': educational_stage_id,
            'periode_config_id': periode_config_id,
            'max_student': self._get_max_student,
        }
        report = self.env['report'].render('school_exams.report_schoolexamlaureat', docargs)
        print "############# hello"
        return report
