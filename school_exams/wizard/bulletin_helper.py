# -*- coding: utf-8 -*-

from odoo import fields,api, models, _
from odoo.exceptions import UserError, ValidationError



class Helper(models.Model):
    _name = 'bulletin.helper'



    name                    = fields.Char('Name ')
    grade_id                = fields.Many2one('school.grade', string="Niveau scolaire")
    annual_calander_id      = fields.Many2one('annual.calendar', string="Année scolaire")
    periode_config_id       = fields.Many2one('periode.config', string="Periode Config", compute='_compute_periode_config')

    class_id                = fields.Many2one('school.class', string="Classe")


    @api.depends('grade_id', 'annual_calander_id')
    @api.multi
    def _compute_periode_config(self):
        for rec in self:
            educational_stage_id = rec.grade_id.educational_stage_id
            domain = [('educational_stage_id','=',educational_stage_id.id),('annual_calander_id','=',rec.annual_calander_id.id)]
            periode_config_id = self.env['periode.config'].search(domain, limit=1)
            rec.periode_config_id = periode_config_id



    
