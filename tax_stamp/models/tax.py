# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
import odoo.addons.decimal_precision as dp

# mapping invoice type to journal type
TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale',
    'in_refund': 'purchase',
}


class TaxStamp(models.Model):
    _name = 'account.tax.stamp'



    @api.model
    def _default_journal(self):
        if self._context.get('default_journal_id', False):
            return self.env['account.journal'].browse(self._context.get('default_journal_id'))
        inv_type = self._context.get('type', 'out_invoice')
        inv_types = inv_type if isinstance(inv_type, list) else [inv_type]
        company_id = self._context.get('company_id', self.env.user.company_id.id)
        domain = [
            ('type', 'in', filter(None, map(TYPE2JOURNAL.get, inv_types))),
            ('company_id', '=', company_id),
        ]
        return self.env['account.journal'].search(domain, limit=1)


    @api.model
    def _default_currency(self):
        journal = self._default_journal()
        return journal.currency_id or journal.company_id.currency_id


    currency_id = fields.Many2one('res.currency', string='Currency',
        required=True, readonly=True, states={'draft': [('readonly', False)]},
        default=_default_currency, track_visibility='always')

    name        = fields.Char(string="Nom")
    value       = fields.Monetary(string="Valeur du Timbre Fiscal", currency_field='currency_id')
    active      = fields.Boolean(string="Activé", default=False)


    @api.model
    def create(self, vals):
        for rec in self.env['account.tax.stamp'].search([]):
            rec.write({'active':False})

        return super(TaxStamp, self).create(vals)


    @api.multi
    def write(self, vals):
        print "### Calling write methode ", vals
        return super(TaxStamp, self).write(vals)


    @api.multi
    def _get_default_taxt_stamp(self):
        return self.env['account.tax.stamp'].search([('active','=',True)], limit=1)



class AccountInvoice(models.Model):
    _inherit = 'account.invoice'


    
    tax_stamp_id        = fields.Many2one('account.tax.stamp',default=lambda self: self.env['account.tax.stamp']._get_default_taxt_stamp())
    tax_stamp_value     = fields.Monetary(related="tax_stamp_id.value", string="Timbre Fiscal",store=True, readonly=True)


    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice')
    def _compute_amount(self):
        print "### Custom __compute_amount "
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_tax = sum(line.amount for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax
        if self.tax_stamp_value:
            self.amount_total += self.tax_stamp_value
            print "### new Amount total ",self.amount_total

        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign