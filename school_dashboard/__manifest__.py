{
    'name': 'School Dashboard',
    'version': '2.0',
    'category': 'School Management',
    'author': 'Odesco Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        
        'views/school_dashboard.xml',
        'views/menuitem.xml',
    ],
    'qweb': ['static/src/xml/*.xml'],

    'installable': True,
    'auto_install': False,
    'application': True,
}