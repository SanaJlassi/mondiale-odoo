odoo.define('school_dashboard.dashboard', function (require) {
"use strict";
var core = require('web.core');
var formats = require('web.formats');
var Model = require('web.Model');
var session = require('web.session');
var ajax = require('web.ajax');
var KanbanView = require('web_kanban.KanbanView');
var KanbanRecord = require('web_kanban.Record');
var ActionManager = require('web.ActionManager');
var QWeb = core.qweb;

var _t = core._t;
var _lt = core._lt;

var SchoolDashboard = KanbanView.extend({
    display_name: _lt('Dashboard'),
    icon: 'fa-dashboard text-red',
    searchview_hidden: true,//  To hide the search and filter bar
    events: {
        'click .total_student': 'action_total_student',
        'click .unpaid_student': 'action_unpaid_student',
        'click .candidat_student': 'action_candidat_student',
        'click .article_to_approve': 'action_article_to_approve',
        'click .parent_msg': 'action_parent_msg',
        'click .waiting_appointment': 'action_waiting_appointment',
        'click .reservation_canteen': 'action_reservation_canteen',
        'click #generate_inscription_pdf': function(){this.generate_pdf("inscriptionChart");},
        'click #generate_unpaid_pdf': function(){this.generate_pdf("unpaidStudentChart")},
        'click #generate_subscription_pdf': function(){this.generate_pdf("subscriptionRequestChart")},
        'click #generate_attendance_day_pdf': function(){this.generate_pdf("attendanceDayChart")},
        'click #generate_attendance_week_pdf': function(){this.generate_pdf("attendanceWeekChart")},
        'click #generate_attendance_month_pdf': function(){this.generate_pdf("attendanceMonthChart")},
        'click .my_profile': 'action_my_profile',
    },
    init: function (parent, dataset, view_id, options) {
        this._super(parent, dataset, view_id, options);
        this.options.creatable = false;
        var uid = dataset.context.uid;
        var employee_data = true;
        var isFirefox = false;
        //Here we can bind any functions to be called before or after render.
        //_.bindAll(this, 'render', 'graph');
        //var _this = this;
        //this.render = _.wrap(this.render, function(render) {
        //    render();
        //    _this.graph();
        //    return _this;
        //});
    },
    fetch_data: function() {
		// Overwrite this function with useful data
		return $.when();
	},
	// Here we are calling a function 'get_school_info' from model to retrieve enough data
    render: function() {
        var super_render = this._super;
        var self = this;
        var model  = new Model('school.dashboard').call('get_school_info').then(function(result){
            self.isFirefox = typeof InstallTrigger !== 'undefined';
            self.employee_data =  result[0]
            return self.fetch_data().then(function(result){
                var school_dashboard = QWeb.render('school_dashboard.dashboard', {
                    widget: self,
                });
                super_render.call(self);
                $(school_dashboard).prependTo(self.$el);
                self.graph();
            })
        });
    },
    action_total_student: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        return this.do_action({
            name: _t("Registred Student"),
            type: 'ir.actions.act_window',
            res_model: 'school.student',
            // src_model: 'school.student',
            view_mode: 'tree,form',
            view_type: 'form',
            views: [[false, 'list'],[false, 'form']],
            context: {},
            domain: [['state','in',['pre-registred','registred']]],
            // search_view_id: self.employee_data.leave_search_view_id,
            target: 'current'
        })
    },
    action_unpaid_student: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        this.do_action({
            name: _t("Unpaid Student"),
            type: 'ir.actions.act_window',
            res_model: 'school.student',
            view_mode: 'tree,form',
            view_type: 'form',
            views: [[false, 'list'],[false, 'form']],
            context: {},
            domain: [['payment_state_group_by','=','unpaid']],
            target: 'current'
        })
    },
    action_candidat_student: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        this.do_action({
            name: _t("Demande d'inscription"),
            type: 'ir.actions.act_window',
            res_model: 'school.student',
            view_mode: 'tree,form',
            view_type: 'form',
            views: [[false, 'list'],[false, 'form']],
            context: {},
            domain: [['state','=','condidate']],
            target: 'current'
        })
    },
    action_article_to_approve: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        this.do_action({
            name: _t("Publication en attente"),
            type: 'ir.actions.act_window',
            res_model: 'school.article',
            view_mode: 'tree,form',
            view_type: 'form',
            views: [[false, 'list'],[false, 'form']],
            context: {},
            domain: [['state','=','unpublished']],
            target: 'current'
        })
    },
    action_parent_msg: function(event) {
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        this.do_action({
            name: _t("Messages des parents en attente de confirmation"),
            type: 'ir.actions.act_window',
            res_model: 'message.sent',
            view_mode: 'tree,form',
            view_type: 'form',
            views: [[false, 'list'],[false, 'form']],
            context: {},
            domain: [['state','=','draft'],],
            // search_view_id: self.employee_data.leave_search_view_id,
            target: 'current'
        })
    },
    action_waiting_appointment: function(event) {
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        this.do_action({
            name: _t("Rendez-vous en attente"),
            type: 'ir.actions.act_window',
            res_model: 'school.appointment',
            view_mode: 'tree,form,calendar',
            view_type: 'form',
            views: [[false, 'list'],[false, 'form'],[false, 'calendar']],
            context: {},
            domain: [['state','=','waiting']],
            target: 'current'
        })
    },
    action_reservation_canteen: function(event){
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        this.do_action({
            name: _t("Réservation cantine en attente"),
            type: 'ir.actions.act_window',
            res_model: 'school.reservation',
            view_mode: 'tree,form',
            view_type: 'form',
            views: [[false, 'list'],[false, 'form']],
            context: {},
            domain: [['state','=','waiting']],
            target: 'current'
        })
    },

    action_my_profile: function(event) {
        var self = this;
        event.stopPropagation();
        event.preventDefault();
        this.do_action({
            name: _t("My Profile"),
            type: 'ir.actions.act_window',
            res_model: 'hr.employee',
            res_id: self.employee_data.id,
            view_mode: 'form',
            view_type: 'form',
            views: [[false, 'form']],
            context: {},
            domain: [],
            target: 'inline'
        })
    },

    // Function which gives random color for charts.
    getRandomColor: function () {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },
    // Here we are plotting bar,pie chart
    graph: function() {
        var self = this

        // Inscription Bar Chart
        var ctx = this.$el.find('#inscriptionChart')
        // Fills the canvas with white background
        Chart.plugins.register({
          beforeDraw: function(chartInstance) {
            var ctx = chartInstance.chart.ctx;
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
          }
        });
        var bg_color_list = []
        for (var i=0;i<=12;i++){
            bg_color_list.push(self.getRandomColor())
        }
        var inscriptionChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["January","February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December"],
                // labels: self.employee_data.payroll_label,
                datasets: [{
                    label: "Nombre des inscriptions par mois",
                    // data: self.employee_data.payroll_dataset,
                    data: [0, 10, 5, 2, 20, 30, 45, 75, 32, 9, 0, 0],
                    backgroundColor: bg_color_list,
                    borderColor: bg_color_list,
                    borderWidth: 1,
                    pointBorderColor: 'white',
                    pointBackgroundColor: 'red',
                    pointRadius: 5,
                    pointHoverRadius: 10,
                    pointHitRadius: 30,
                    pointBorderWidth: 2,
                    pointStyle: 'rectRounded'
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            // min: 0,
                            // max: Math.max.apply(null,self.employee_data.payroll_dataset),
                            min: 0,
                            max: 100,
                            // stepSize: self.employee_data.
                            // payroll_dataset.reduce((pv,cv)=>{return pv + (parseFloat(cv)||0)},0)
                            // /self.employee_data.payroll_dataset.length
                          }
                    }]
                },
                responsive: true,
                maintainAspectRatio: true,
                animation: {
                    duration: 100, // general animation time
                },
                hover: {
                    animationDuration: 500, // duration of animations when hovering an item
                },
                responsiveAnimationDuration: 500, // animation duration after a resize
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'black'
                    }
                },
            },
        });

        // Unpaid Student Bar chart
        var unpaidStudent = this.$el.find('#unpaidStudentChart')
        var bg_color_list = []
        for (var i=0;i<=12;i++){
            bg_color_list.push(self.getRandomColor())
        }
        var unpaidStudentChart = new Chart(unpaidStudent, {
            type : 'bar',
            data : {
                labels: ["January","February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December"],
                datasets: [{
                    label: "Nombre des impayés par mois",
                    // data: self.employee_data.payroll_dataset,
                    data: [13, 20, 22, 30, 45, 75, 82, 0, 0, 0, 3, 10],
                    backgroundColor: bg_color_list,
                    borderColor: bg_color_list,
                    borderWidth: 1,
                    pointBorderColor: 'white',
                    pointBackgroundColor: 'red',
                    pointRadius: 5,
                    pointHoverRadius: 10,
                    pointHitRadius: 30,
                    pointBorderWidth: 2,
                    pointStyle: 'rectRounded'
                }] 
            },
            options : {
                scales:{
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 100,
                          }
                    }],

                },
                responsive: true,
                maintainAspectRatio: true,
                animation: {
                    duration: 100, // general animation time
                },
                hover: {
                    animationDuration: 500, // duration of animations when hovering an item
                },
                responsiveAnimationDuration: 500, // animation duration after a resize
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'black'
                    }
                },
            },
            
        });

        // Subscription request Bar chart
        var subscriptionRequest = this.$el.find('#subscriptionRequestChart')
        var bg_color_list = []
        for (var i=0;i<=12;i++){
            bg_color_list.push(self.getRandomColor())
        }
        var subscriptionRequestChart = new Chart(subscriptionRequest, {
            type : 'bar',
            data : {
                labels: ["January","February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December"],
                datasets: [{
                    label: "Demandes d'inscription par mois",
                    // data: self.employee_data.payroll_dataset,
                    data: [0, 10, 5, 2, 20, 30, 45, 75, 32, 9, 0, 0],
                    backgroundColor: bg_color_list,
                    borderColor: bg_color_list,
                    borderWidth: 1,
                    pointBorderColor: 'white',
                    pointBackgroundColor: 'red',
                    pointRadius: 5,
                    pointHoverRadius: 10,
                    pointHitRadius: 30,
                    pointBorderWidth: 2,
                    pointStyle: 'rectRounded'
                }] 
            },
            options : {
                scales:{
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 100,
                          }
                    }],

                },
                responsive: true,
                maintainAspectRatio: true,
                animation: {
                    duration: 100, // general animation time
                },
                hover: {
                    animationDuration: 500, // duration of animations when hovering an item
                },
                responsiveAnimationDuration: 500, // animation duration after a resize
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'black'
                    }
                },
            },
            
        });

        // Attendance Day Doughnut Chart
        var attendanceDay = this.$el.find('#attendanceDayChart');
        bg_color_list = []
        for (var i=0;i<=3;i++){
            bg_color_list.push(self.getRandomColor())
        }
        var attendanceDayChart = new Chart(attendanceDay, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [20,40,60],
                    backgroundColor: bg_color_list,
                    label: 'Assiduité par jour'
                }],
                labels:['Absents','Retards','Présents'],
            },
            options: {
                responsive: true
            }
        });

        // Attendance Week Doughnut Chart
        var attendanceWeek = this.$el.find('#attendanceWeekChart');
        var attendanceWeekChart = new Chart(attendanceWeek, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [10,50,40],
                    backgroundColor: bg_color_list,
                    label: 'Assiduité par semaine'
                }],
                labels:['Absents','Retards','Présents'],
            },
            options: {
                responsive: true
            }
        });

        // Attendance Month Doughnut Chart
        var attendanceMonth = this.$el.find('#attendanceMonthChart');
        var attendanceMonthChart = new Chart(attendanceMonth, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [20,55,80],
                    backgroundColor: bg_color_list,
                    label: 'Assiduité par mois'
                }],
                labels:['Absents','Retards','Présents'],
            },
            options: {
                responsive: true
            }
        });



        // $('#emp_details').DataTable( {
        //     dom: 'Bfrtip',
        //     buttons: [
        //         'copy', 'csv', 'excel',
        //         {
        //             extend: 'pdf',
        //             footer: 'true',
        //             orientation: 'landscape',
        //             title:'Employee Details',
        //             text: 'PDF',
        //             exportOptions: {
        //                 modifier: {
        //                     selected: true
        //                 }
        //             }
        //         },
        //         {
        //             extend: 'print',
        //             exportOptions: {
        //             columns: ':visible'
        //             }
        //         },
        //     'colvis'
        //     ],
        //     columnDefs: [ {
        //         targets: -1,
        //         visible: false
        //     } ],
        //     lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        //     pageLength: 15,
        // } );
    },
    generate_pdf: function(chart){
        if (chart == 'inscriptionChart'){
            var canvas = document.querySelector('#inscriptionChart');
        }
        else if (chart == 'unpaidStudentChart') {
            var canvas = document.querySelector('#unpaidStudentChart');
        }
        else if (chart == 'subscriptionRequestChart') {
            var canvas = document.querySelector('#subscriptionRequestChart');
        }
        else if (chart == 'attendanceDayChart') {
            var canvas = document.querySelector('#attendanceDayChart');
        }
        else if (chart == 'attendanceWeekChart') {
            var canvas = document.querySelector('#attendanceWeekChart');
        }
        else if (chart == 'attendanceMonthChart') {
            var canvas = document.querySelector('#attendanceMonthChart');
        }
        

        //creates image
        var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150 );
        doc.save('report.pdf');
    },
})
// View adding to the registry
core.view_registry.add('school_dashboard_view', SchoolDashboard);
return SchoolDashboard
});
