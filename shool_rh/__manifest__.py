{
    'name': 'School RH',
    'version': '2.0',
    'category': 'School Management',
    'author': 'Odesco Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [

    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}