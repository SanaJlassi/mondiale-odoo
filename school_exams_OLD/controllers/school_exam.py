# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)

class SchoolExam(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/exams', type='http', auth="user", methods=['GET'],  csrf=False)
    def exams(self, **fields):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Teacher found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        exam_id = request.env['school.exam'].search(['&',('teacher_id','=',teacher_id.id),('state','=','approved')])
        exam_list = []
        for exam in exam_id:
            exam_json = {
                'id':exam.id,
                'from_date':exam.from_date,
                'to_date':exam.to_date,
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },
                'exam_type':{
                    'id':exam.exam_type.id,
                    'name':exam.exam_type.name,
                    'coefficient':exam.exam_type.coefficient,
                },
                'description':exam.description,
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)    



    @http.route('/api/exams/class/<int:class_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def exams_by_class(self, class_id, **fields):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Teacher found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        exam_id = request.env['school.exam'].search(['&',('teacher_id','=',teacher_id.id),'&',('class_id','=',class_id),('state','=','approved')])
        exam_list = []
        for exam in exam_id:
            exam_json = {
                'id':exam.id,
                'from_date':exam.from_date,
                'to_date':exam.to_date,
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },
                'exam_type':{
                    'id':exam.exam_type.id,
                    'name':exam.exam_type.name,
                    'coefficient':exam.exam_type.coefficient,
                },
                'description':exam.description,
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)    


    

    @http.route('/api/exams/<int:exam_id>/notes', type='http', auth="user", methods=['POST'],  csrf=False)
    def exams_note(self, exam_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
                       
        teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])        
        json_object = None
        if not request.params.get('notes',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'notes object not submited'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        exam_id = request.env['school.exam'].search([('id','=',exam_id)])
        if not exam_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No exam found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        # print "### DATA RECIEVED : ",request.params.get('task',False)
        #_logger.warning('### DATA RECIEVED :  (%s).', request.params.get('task',False))
        try:
            json_object = json.loads(request.params.get('notes',False))
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize task object'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        list_note = []
        for el in json_object:
            student_id  = request.env['school.student'].search([('id','=',el['id'])])
            if not student_id:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No Student found with the given id %s'%el['id']} 
                return http.request.make_response(json.dumps(response),SchoolExam.HEADER)                
            list_note.append((0,0,{'name':'name','student_id':student_id.id, 'note':el.get('note',0), 'comment':el.get('comment',False)}))

        exam_note_vals = {'exam_id':exam_id.id,'exam_notes_line_ids':list_note}
        record = request.env['school.exam.note'].create(exam_note_vals)
        if not record:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Error while creating object exam notes'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)                

        response['success'] = True
        response['data'] = None
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)   


        


        


        # json_object = {
        #     'grade_id':json_object['grade_id'],
        #     'subject_id':json_object['subject_id'],
        #     'teacher_id':teacher.id,
        #     'name':json_object['name'],
        #     'academic_year_id':request.env['academic.year'].search([('active_year','=',True)]).id,
        #     'description':json_object['description'],
        #     'dead_line':date,
        #     'class_id':json_object['class_id'],
        #     'timing_system_periode_id':json_object['timing_system_periode_id'],
        #     'all_student':json_object['all_student'],
        #     'student_ids':list_student,
        #     'file_ids':self._prepar_file_objects(json_object['file_ids']),
        # }
        # context = request.env.context.copy()
        # context['source'] = 'controller'
        # request.env.context = context
        # res = request.env['school.task'].create(json_object)
        response['success'] = False
        response['data'] = False
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)








    # @http.route('/api/subject/exam/<int:id>', type='http', auth="user", methods=['GET'],  csrf=False)
    # def exams(self,id, **fields):
    #   response = {'success':False, 'data':None}
    #   if not request.env.user.has_group('genext_school.group_school_teacher'):
    #       response['success'] = False
    #       response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
    #       return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

    #   teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
    #   if not teacher_id:
    #       response['success'] = False
    #       response['error'] = {'code':404, 'message':'No Teacher found with the given id'} 
    #       return http.request.make_response(json.dumps(response),SchoolExam.HEADER)


        







