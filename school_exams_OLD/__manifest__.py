{
    'name': 'Exams',
    'version': '1.0',
    'category': 'Scolarité',
    'author': 'Genext Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        'security/ir.model.access.csv',
        'views/school_exam_view.xml',
        'report/bulletin_report.xml',
        'report/bulletin_report_template.xml',
        'report/report_template_layout.xml',

    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}