# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)

class SchoolExam(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/exams', type='http', auth="user", methods=['GET'],  csrf=False)
    def exams(self, **fields):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        student_id = request.env['school.student'].search([('user_id.id','=',request.env.user.id)])
        _logger.warning("Profile student %s",len(student_id))
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        #exam_id = request.env['school.exam'].search(['&',('teacher_id','=',teacher_id.id),('state','=','approved')])
        exam_id = request.env['school.exam'].search([('class_id','=',student_id.class_id.id)])
        _logger.warning('### EXAM ID :  %s', exam_id)
        exam_list = []
        for exam in exam_id:
            exam_json = {
                'id':exam.id,
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },
                'academic_year_id': exam.academic_year.id,
                'timing_system_periode_id': exam.timing_system_periode_id.id,
                'exam_lines': self._load_exam_lines_as_json(exam, 'student', student_id),
                'exam_moyenne_lines': self._load_exam_moyenne_lines_as_json(exam, 'student', student_id),
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 
 



    @http.route('/api/exams/class/<int:class_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def exams_by_class(self, class_id, **fields):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Teacher found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        #exam_id = request.env['school.exam'].search(['&',('teacher_id','=',teacher_id.id),'&',('class_id','=',class_id),('state','=','approved')])
        exam_id = request.env['school.exam'].search(['&',('teacher_id','=',teacher_id.id),('class_id','=',class_id)])
        exam_list = []
        for exam in exam_id:
            exam_json = {
                'id':exam.id,
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },
                'academic_year_id': exam.academic_year.id,
                'timing_system_periode_id': exam.timing_system_periode_id.id,
                'exam_lines': self._load_exam_lines_as_json(exam, 'teacher', teacher_id),
                'exam_moyenne_lines': self._load_exam_moyenne_lines_as_json(exam, 'teacher', teacher_id),
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 

    # returns a list of exam lines dict 
    def _load_exam_lines_as_json(self, exam, user_type, user_id):
        exam_line = []
        for line in exam.exam_line_ids:
            #print "EXAM LINE ",line.name
            if line.state == 'approved':
	            exam_line.append({
	                'id':line.id, 
	                'name':line.name, 
	                'type':line.exam_type.id, 
	                'session': line.exam_session, 
	                'from_date': line.from_date, 
	                'to_date': line.to_date,
	                'description': line.description,
	                'scale': line.scale ,
	                'exam_notes_line_ids': self._load_exam_line_notes_as_json(line, user_type, user_id),
	                })
        return exam_line

    # returns a list of exam moyennes lines dict 
    def _load_exam_moyenne_lines_as_json(self, exam, user_type, user_id):
        moyenne_line = []
        if user_type == 'teacher':
            for line in exam.exam_moyenne_ids:
                moyenne_line.append({
                    'id':line.id, 
                    #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'moyenne': line.moyenne, 
                    'result': line.result, 
                    })
        if user_type == 'student':
            for line in exam.exam_moyenne_ids:
                if line.student_id.id == user_id.id:
                    moyenne_line.append({
                    'id':line.id, 
                    #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'moyenne': line.moyenne, 
                    'result': line.result, 
                    })

        return moyenne_line


    # returns a list of exam lines marks dict 
    def _load_exam_line_notes_as_json(self, line, user_type, user_id):
        exam_line_note = []
        if user_type == 'teacher':
            for i in line.exam_notes_line_ids:
                exam_line_note.append({
                    'id':i.id, 
                    'student_id':i.student_id.id, 
                    'name': i.student_id.name,
                    'last_name': i.student_id.last_name,
                    'note': i.note, 
                    'comment': i.comment, 
                    })
        if user_type == 'student':
            for i in line.exam_notes_line_ids:
                if i.student_id.id == user_id.id:
                    exam_line_note.append({
                    'id':i.id, 
                    'student_id':i.student_id.id, 
                    'name': i.student_id.name,
                    'last_name': i.student_id.last_name,
                    'note': i.note, 
                    'comment': i.comment, 
                    })

        return exam_line_note
    

    @http.route('/api/exams/<int:exam_line_id>/notes', type='http', auth="user", methods=['POST'],  csrf=False)
    def exams_note(self, exam_line_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
                       
        teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        _logger.warning('### DATAAAAAAA :  (%s).', request.params.get('notes',False))       
        json_object = None
        if not request.params.get('notes',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'notes object not submited'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        exam_line = request.env['school.exam.line'].search([('id','=',exam_line_id)])
        if not exam_line:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No exam found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        json_object = None
        try:
            json_object = json.loads(request.params.get('notes',False))
            _logger.warning('###  JSON-OBJECT  :  %s', json_object)
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize notes object'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        # if self._validate_object(json_object, response)['valide'] == False:
        #     response.pop('valide', None)
        #     return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        # else:
        #     response.pop('valide', None)

        _logger.warning('### JSON-OBJECT  :  (%s).', json_object)
        list_note = []
        for el in json_object:
            _logger.warning('### JSON-OBJECT el :  (%s).', el)
            rec = request.env['school.exam.note.line'].browse(el['id'])
            res = rec.write({'note':el['note'],'comment':el['comment']})
            if not res:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Error while updating note'} 
                return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
            else:
                response['success'] = True
                response['data'] = False


        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)










        







