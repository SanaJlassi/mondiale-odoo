# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)

class ReportExamWizard(models.TransientModel):
    _name = "report.exam.wizard"
    _description = "Report Exam wizard"

    grade_id                    = fields.Many2one('school.grade',string="Niveau scolaire", required=True)
    class_id                    = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)
    academic_year               = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True, string="Année scolaire")
    educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    timing_system_id            = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
    timing_system_periode_id    = fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system_id)]", ondelete='set null', string="Semestre", required=True)
    exam_session                = fields.Selection([('rattrapage','Rattrapage'),('principale','Principale')], default='principale', string="Session")
    # student_ids                 = fields.One2many('school.student', 'class_id', compute='_compute_students')

    @api.model
    def create(self, vals):
        _logger.warning("####### CREATE ")
        return super(ReportExamWizard,self).create(vals)

    @api.multi
    def check_report(self):
        data = {}
        _logger.warning("####### check_report 1")
        data['form'] = self.read(['grade_id','class_id','academic_year','timing_system_id','timing_system_periode_id','educational_stage_id','exam_session'])[0]
        # data['form'].update({
        #     'grade_id':self.grade_id.id,
        #     'class_id':self.class_id.id,
        #     'academic_year':self.academic_year.id,
        #     'timing_system_id':self.timing_system_id.id,
        #     'timing_system_periode_id':self.timing_system_periode_id.id,
        #     'educational_stage_id':self.educational_stage_id.id,
        #     'exam_session':self.exam_session.id
        #     }
        #     )
        #print "######### DATA ",data
        _logger.warning("####### check_report 2")
        return self._print_report(data)

    def _print_report(self, data):
        _logger.warning("####### _print_report ")
        # data['form'].update({
        #     'grade_id':self.grade_id.id,
        #     'class_id':self.class_id.id,
        #     'academic_year':self.academic_year.id,
        #     'timing_system_id':self.timing_system_id.id,
        #     'timing_system_periode_id':self.timing_system_periode_id.id,
        #     'educational_stage_id':self.educational_stage_id.id,
        #     'exam_session':self.exam_session.id
        #     }
        #     )
        data['form'].update(self.read(['grade_id','class_id','academic_year','timing_system_id','timing_system_periode_id','educational_stage_id','exam_session'])[0])
        #print "####### UPDATED DATA ",data
        return self.env['report'].get_action(self, 'school_exams.report_exam', data=data)



class StateInvoiceReport(models.AbstractModel):
    _name = 'report.school_exams.report_exam'

    # def _get_subject(self, grade_id):
    #     subject_list = []
    #     grade = self.env['school.grade'].search([('id','=',grade_id.id)])
    #     if len(grade):
    #         for subject in grade.subject_ids:
    #         subject_list = subject.subject_id
    #     return subject_list

    # def _get_exam(self, grade_id, class_id, academic_year, semester):
    #     exam_list = []
    #     exam_list = self.env['school.exam'].search([('grade_id','=',docs.grade_id.id),('class_id','=',docs.class_id.id),('academic_year','=',docs.academic_year.id),('timing_system_periode_id','=',docs.timing_system_periode_id.id)])
    #     if len(exam_list):
    #         teacher_id = note_list[0].teache_id
    #         teacher_name = teacher_id.name +' '+ teacher_id.last_name
    #         return teacher_name
    #     else:
    #         return '-'

    @api.model
    def render_html(self, docids, data=None):
        self.model  = self.env.context.get('active_model')
        docs        = self.env[self.model].browse(self.env.context.get('active_id'))
        students    = self.env["school.student"].search([("class_id", "=",docs.class_id.id)])
        exam_type   = self.env['school.exam.type'].search([])
        _logger.warning("####### exam type %s ",exam_type)
        exam_lines  = self.env['school.exam.line'].search(['&',('class_id','=',docs.class_id.id),'&',('academic_year','=',docs.academic_year.id),'&',('timing_system_periode_id','=',docs.timing_system_periode_id.id),('exam_session','=',docs.exam_session)])
        
        exam_ids    = self.env['school.exam'].search([('grade_id','=',docs.grade_id.id),('class_id','=',docs.class_id.id),('academic_year','=',docs.academic_year.id),('timing_system_periode_id','=',docs.timing_system_periode_id.id)])

        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            #'invoices': invoices,
            'students': students,
            'exam_lines': exam_lines,
        }
        _logger.warning("####### render_html ")
        #print "################# docargs ",docargs
        return self.env['report'].render('school_exams.report_exam', docargs)