{
    'name': 'Exams University',
    'version': '1.0',
    'category': 'Scolarité',
    'author': 'Odesco Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        'security/ir.model.access.csv',
        'views/school_exam_view.xml',
        'wizard/report_exam.xml',
        'wizard/report_exam_template.xml',
        'report/report.xml',
        'report/catch_up_list_report.xml',


    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}