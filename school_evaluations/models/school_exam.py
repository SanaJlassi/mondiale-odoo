# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class Exam(models.Model):
    _name = 'school.exam'
    _description = 'Exam'

    @api.depends('class_id','subject_id')
    @api.multi
    def _compute_name(self):
        for record in self:
            record.name = record.class_id.code+'/'+record.subject_id.name


    name                        = fields.Char(string="Nom", compute=_compute_name ,required=True)

    academic_year               = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True)
    grade_id                    = fields.Many2one('school.grade',  string='Niveau scolaire', required=True)
    class_id                    = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", string="Classe", required=True)
    subject_id                  = fields.Many2one('school.subject',  string='Matière', required=True)
    teacher_id                  = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", ondelete='set null', string="Enseignant", required=True)

    educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    timing_system_id            = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
    timing_system_periode_id    = fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system_id)]", ondelete='set null', string="Semestre", required=True)
    
    exam_line_ids               = fields.One2many('school.exam.line', 'exam_id', ondelete='cascade')
    exam_moyenne_ids            = fields.One2many('school.exam.moyenne', 'exam_id', ondelete='cascade')

    _sql_constraints = [('exam_unique', 'unique(class_id, subject_id, timing_system_periode_id)',"L'examen doit être unique!")]

    @api.onchange('class_id')
    def _onchange_class_id(self):
        exam_moyenne_ids = []
        students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
        for student in students:
            exam_moyenne_ids.append((0,0,{'name':'name','student_id':student.id, 'moyenne': 0.0, 'result': 'pass'}))        
        return {'value':{'exam_moyenne_ids':exam_moyenne_ids}}





    
    # @api.depends('exam_line_ids')
    # @api.multi
    # def get_notes(self):
    #     list_moyenne = []
    #     if len(self.exam_line_ids):
    #         notes = self.env['school.exam.note.line'].search(['&',('exam_line_id.exam_id','=',self.name),'&',('exam_line_id.academic_year','=',self.academic_year.id),('exam_line_id.timing_system_periode_id','=',self.timing_system_periode_id.id)])
    #         _logger.warning("######### exam line note %s", notes )





class ExamLine(models.Model):
    _name = 'school.exam.line'
    _description = 'Exam Line'

    @api.depends('exam_id','exam_type')
    @api.multi
    def _compute_name(self):
        for record in self:
            record.name = record.exam_type.name+'/'+record.exam_id.name

    name                        = fields.Char(string="Nom", compute=_compute_name)

    exam_id                     = fields.Many2one('school.exam', ondelete='cascade', string="Examen", required=True)
    exam_type                   = fields.Many2one('school.exam.type',  string="Type", required=True)
    exam_session                = fields.Selection([('rattrapage','Rattrapage'),('principale','Principale')], default='principale', string="Session")
    scale                       = fields.Float(string="Barème", default=0.0)
    state                       = fields.Selection([('unapproved','Non approuvé'),('approved','Approuvé'),], default='unapproved', string="état")
    from_date                   = fields.Datetime(string="Date de début", default=fields.Datetime.now, required=True)
    to_date                     = fields.Datetime(string="Date de fin", default=fields.Datetime.now, required=True)
    description                 = fields.Char(string="Description de l'examen", default="Description")

    academic_year               = fields.Many2one(related='exam_id.academic_year', required=True)
    grade_id                    = fields.Many2one(related='exam_id.grade_id', string='Niveau scolaire', required=True)    
    class_id                    = fields.Many2one(related='exam_id.class_id', string='Classe', required=True, store=True)
    educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    timing_system_id            = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
    timing_system_periode_id    = fields.Many2one(related='exam_id.timing_system_periode_id', string='Semestre', required=True, store=True)
    subject_id                  = fields.Many2one(related='exam_id.subject_id', string='Matière', required=True, store=True)
    teacher_id                  = fields.Many2one(related='exam_id.teacher_id', string='Enseignant', required=True)

    exam_notes_line_ids         = fields.One2many('school.exam.note.line', 'exam_line_id', ondelete='cascade')

    _sql_constraints = [('approved_exam_unique', 'unique(exam_id, state="approved")',"Les notes sont déjà saisies")]

    @api.one
    def set_unapproved(self):
        self.write({'state':'unapproved'})        

    @api.one
    def set_approved(self):  
        self.write({'state':'approved'})


    @api.onchange('exam_type')
    def _onchange_exam_type(self):
        student_line= []
        _logger.warning("#########   on change exam type"  ) 
        if len(self.class_id):
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                student_line.append( (0,0,{'name':'name','student_id':student.id, 'note':None, 'comment':None}) )
        else:
            _logger.warning("###### NO CLASS ID"  )  
        return {'value':{'exam_notes_line_ids':student_line}}
 


class ExamType(models.Model):
    _name = 'school.exam.type'
    _description = 'Exam Type'

    name                        = fields.Char(string="Type de l'examen", required=True)
    coefficient                 = fields.Float(string='Coefficient', default=0.0, required=True)
    evaluated                   = fields.Boolean(string="A évaluer", default=False)


class ExamMoyenne(models.Model):
    _name = 'school.exam.moyenne'
    _description = 'Exam moyenne'

    @api.multi
    def _compute_moyenne(self):
        for rec in self:
            # _logger.warning("######### _compute_moyenne %s", rec.exam_id )
            exam_line_ids = rec.exam_id.mapped('exam_line_ids')
            total = 0.0 
            div = 0.0 
            for exam in exam_line_ids:
                if exam.state == 'approved':
                    # _logger.warning("######### exam_line_ids %s", exam.exam_type.name )
                    exam_notes_line_ids = exam.mapped('exam_notes_line_ids').filtered(lambda r:r.student_id == rec.student_id)
                    # _logger.warning("######### exam_notes_line_ids %s", exam_notes_line_ids.note )
                    total += exam_notes_line_ids.note * exam.exam_type.coefficient
                    div += exam.exam_type.coefficient
            if div == 0:
                div = 1
            # _logger.warning("######### total %s", total )
            # _logger.warning("######### div %s", div )
            rec.moyenne = total/div
            # _logger.warning("######### moyenne %s", rec.moyenne )
            if rec.moyenne < 10:
                rec.write({'result':'fail'})
            if rec.moyenne >= 10:
                rec.write({'result':'pass'})


    student_id                  = fields.Many2one('school.student', string="Élève")
    exam_id                     = fields.Many2one('school.exam', ondelete='cascade', string="Examen", required=True)
    moyenne                     = fields.Float('Moyenne', default=0.0,  compute=_compute_moyenne)
    result                      = fields.Selection([('pass','Passe'),('fail','Rattrapage')])



    



# class ExamNote(models.Model):
#     _name = 'school.exam.note'
#     _description = 'Exam Note'


#     exam_id                     = fields.Many2one('school.exam', ondelete='cascade', string="Nom de l'examen", required=True )
#     subject_id                  = fields.Many2one(related='exam_id.subject_id', string='Matière', required=True)

#     academic_year               = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True)
#     grade_id                    = fields.Many2one(related='exam_id.grade_id', string='Niveau scolaire', required=True)    
#     class_id                    = fields.Many2one(related='exam_id.class_id', string='Classe', required=True)
#     educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
#     timing_system_id            = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
#     period                      = fields.Many2one(related='exam_id.timing_system_periode_id', string='Période', required=True)
#     session                     = fields.Selection([('rattrapage','Rattrapage'),('principale','Principale')], default='principale', string="Session")
    
#     teacher_id                  = fields.Many2one(related='exam_id.teacher_id', string='Enseignant', required=True)
#     student_ids                 = fields.Many2many('school.student', string="Élève", domain="[('class_id','=',class_id)]", required=True)

#     state                       = fields.Selection([('unapproved','Non approuvé'),
#                                                                  ('approved','Approuvé'),
#                                                                  ('ignored','Ignoré'),
#                                                                  ], default='unapproved', string="état")

#     moyenne                     = fields.Float(string="La moyenne de la classe", default=0.0)

#     exam_notes_line_ids         = fields.One2many('school.exam.note.line', 'exam_notes_id')

#     @api.one
#     def set_unapproved(self):
#         self.write({'state':'unapproved'})        

#     @api.one
#     def set_approved(self):  
#         self.write({'state':'approved'})


#     @api.one
#     def set_ignore(self):
#         self.write({'state':'ignored'})

#     @api.onchange('class_id')
#     def _onchange_class_id(self):
#         student_line= []
#         if len(self.class_id):
#             students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
#             for student in students:
#                 student_line.append( (0,0,{'name':'name','student_id':student.id, 'note':None, 'comment':None}) )
#         else:
#             print "NO CLASS ID"    
#         return {'value':{'exam_notes_line_ids':student_line}}




class ExamNoteLine(models.Model):
    _name = 'school.exam.note.line'
    _description = 'Exam Notes Line'

    # name                        = fields.Char(string="Note",default="exam note line",readonly=True)
    exam_line_id                = fields.Many2one('school.exam.line', ondelete='cascade', string="Examen", required=True )
    #exam_notes_id               = fields.Many2one('school.exam.note')
    student_id                  = fields.Many2one('school.student', string="Stagiaire")
    note                        = fields.Float(string='Note', default=0.0)
    comment                     = fields.Char(string="Commentaire")

# class StudentResult(models.Model):
#     _name = 'school.student.result'
#     _description = 'Student Result'