{
    'name': 'School Timetable',
    'version': '2.0',
    'category': 'School Management',
    'author': 'Odesco Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        # 'views/timetable_view.xml',
        'views/menuitem.xml',
        

    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}