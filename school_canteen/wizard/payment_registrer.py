# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)



class RegisterPayment(models.TransientModel):
    _name = 'cantine.payment.register'
    _inherit = 'account.payment'

    total_amount 		= fields.Float(string='Total')




    @api.model 
    def default_get(self, fields):
        rec = super(RegisterPayment, self).default_get(fields)
        context = dict(self._context or {})
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')


        _logger.warning('###### len of records %s',len(active_ids))
        # Checks on context parameters
        if not active_model or not active_ids:
            raise UserError(_("Programmation error: wizard action executed without active_model or active_ids in context."))
        if active_model != 'school.reservation':
            raise UserError(_("Programmation error: the expected model for this action is 'school.reservation'. The provided one is '%d'.") % active_model)

        # Checks on received invoice records
        reservation_id = self.env[active_model].browse(active_ids)
        if any(record.state != 'accepted' for record in reservation_id):
            raise UserError(_("Vous pouvez uniquement enregistrer les paiements pour les réservations acceptées"))
        if any(record.student_id.id != reservation_id[0].student_id.id for record in reservation_id):
            raise UserError(_("Vous pouvez générer un paiement pour un élève seulement !."))
        
        total_amount = sum(rec.reservation_type_id.price for rec in reservation_id)
        payment_lines = []
        for record in reservation_id:
            line = {}

        rec.update({
            'total_amount':total_amount,
            'state':'draft',
            'student_id':reservation_id[0].student_id.id,
        })
        return rec


    @api.multi
    def create_payment(self):
    	for rec in self:
    		print "Hello world"
    	return True