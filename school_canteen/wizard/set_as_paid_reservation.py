# -*- coding: utf-8 -*-
from odoo import models, api, _
from odoo.exceptions import UserError


class SchoolReservationConfirm(models.TransientModel):
    """
    This wizard will confirm the paiement of all selected reservations
    """

    _name = "school.reservation.confirm"
    _description = "Confirm the selected reservations"

    @api.multi
    def reservation_confirm(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['school.reservation'].browse(active_ids):
            if record.state not in ('accepted'):
                raise UserError(_("Selected reservation(s) cannot be confirmed as paid as they are not in 'Accepted' state."))
            record.set_as_paid()
        return {'type': 'ir.actions.act_window_close'}