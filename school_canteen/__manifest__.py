{
    'name': "Cantine",
    'version': '1.0',
    'category': 'Scolarité',
    'author': 'Ben Selma Sghaier',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school','report'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'report/report.xml',
        'views/cantine_report.xml',
        'views/school_canteen_view.xml',
        'wizard/cantine_report_wizard.xml',
        'wizard/payment_registrer_view.xml',
        'wizard/set_as_paid_reservation.xml',
    ],
    #'qweb': ['static/src/xml/*.xml'],

    'installable': True,
    'auto_install': False,
    'application': True,
}