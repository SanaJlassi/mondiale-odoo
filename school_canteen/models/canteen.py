# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
import locale
import calendar
from datetime import date, datetime



DAYS_IN_WORD = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']


class Meal(models.Model):
    _name = 'school.meal'
    _description = 'Meal'


    name                        = fields.Char(string='Nom du repas', required=True)
    meal_type_id                = fields.Many2one('school.meal.type', string="Type de repas", required=True, ondelete="set null")


class MealType(models.Model):
    _name = 'school.meal.type'
    _description = 'Meal Type'


    name                        = fields.Char(string='Nom', required=True)




class Menu(models.Model):
    _name = 'school.menu'
    _description = 'Menu'


    name                        = fields.Char(string='Nom de menu')
    active                      = fields.Boolean(string="actif", default=True)
    menu_line_ids               = fields.One2many('school.menu.line', 'menu_id', string='Repas', required=True)

    from_date                   = fields.Date(string="Date", default=fields.Date.today(), required=True)
    to_date                     = fields.Date(string="to_datetime", default=fields.Date.today())
    user_id                     = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)


    @api.model
    def create(self, vals):
        res = super(Menu,self).create(vals)
        dow = datetime.strptime(res.from_date, DEFAULT_SERVER_DATE_FORMAT).date().weekday()
        date = datetime.strptime(res.from_date, DEFAULT_SERVER_DATE_FORMAT).date()
        name = DAYS_IN_WORD[dow]+" "+str(date.day)+"/"+str(date.month)
        res.write({'name':name})
        return res

    @api.multi
    def write(self, vals):
        res = super(Menu,self).write(vals)
        for rec in self:
            if res and vals.get('from_date', False):
                dow = datetime.strptime(rec.from_date, DEFAULT_SERVER_DATE_FORMAT).date().weekday()
                date = datetime.strptime(rec.from_date, DEFAULT_SERVER_DATE_FORMAT).date()
                name = DAYS_IN_WORD[dow]+" "+str(date.day)+"/"+str(date.month)
                rec.write({'name':name})
        return res

    @api.multi
    def unlink(self):
        print "##### deleting"
        print "##### contxt",self._context
        return super(Menu,self).unlink()
        
    
    
class MenuLine(models.Model):
    _name = 'school.menu.line'
    _description = 'Menu Line'


    name                        = fields.Char(string="description")
    menu_id                     = fields.Many2one('school.menu', ondelete='cascade', required=True, )
    meal_type_id                = fields.Many2one('school.meal.type', string="Type du repas", required=True)
    meal_id                     = fields.Many2one('school.meal', string="Repas", damain="[('meal_type_id','=',meal_type_id)]", required=True)


    @api.onchange('meal_type_id', 'meal_id')
    @api.multi
    def _onchange_line(self):
        return {'domain':{'meal_id':[('meal_type_id','=',self.meal_type_id.id)]}}



class Reservation(models.Model):
    _name = 'school.reservation'
    _description = 'Reservation of a menu'


    name                        = fields.Char(string="Code", readonly=True)
    student_id                  = fields.Many2one('school.student', string="Élève", required=True, ondelete="set null")
    menu_id                     = fields.Many2one('school.menu', string='Menu', required=True, ondelete="set null")
    reservation_type_id         = fields.Many2one('school.reservation.type', string="Type de réservation", required=True, ondelete="set null")
    start_date                  = fields.Date(string='Date début', default=fields.Date.today())
    end_date                    = fields.Date(string='Date fin')
    class_id                    = fields.Many2one('school.class', compute="_compute_class", store=True, string="Classe")

    state                       = fields.Selection([
                                                    ('waiting','En attente'),
                                                    ('accepted','Accepté'),
                                                    ('paid','Payé'),
                                                    ('rejected','Rejeté'),
                                                    ('canceled','Annulé'),
                                                    ], default='waiting')

    @api.multi
    @api.depends('student_id')
    def _compute_class(self):
        for rec in self:
            rec.class_id = rec.student_id.class_id
        return True


    @api.multi
    def set_waiting(self):
        self.write({'state':'waiting'})
        return True

    @api.multi
    def set_accepted(self):
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            ''' foreach sanction in [self --> recordset of sanction, can contain one record or a set of records]'''
            for rec in self:
                tokens = []
                for parent in rec.student_id.parent_ids:
                    for token in parent.token_ids:
                        print "#### TOKEN ",parent.token
                        tokens.append(token.token)
                        reservation = {
                            'id': rec.id,
                            'name':rec.name,
                            'state':rec.state,
                            'student_name':rec.student_id.name,
                            'student_last_name':rec.student_id.last_name,
                            'type':rec.reservation_type_id.name,
                            'menu':{
                                'id':rec.menu_id.id,
                                'name':rec.menu_id.name,
                                'date':rec.menu_id.from_date,
                                # 'content':self._load_meals_by_menu(res.menu_id),
                            }
                        }

                        data = {'module':'CanteenPage','data':reservation, 'kid':{'id':rec.student_id.id,'name':rec.student_id.name,'last_name':rec.student_id.last_name}}
                        content = {'en':'Your reservation canteen for %s is confirmed [%s]'%(rec.menu_id.name, rec.student_id.name), 
                                   'fr':'Votre réservation cantine pour %s est confirmée [%s]'%(rec.menu_id.name, rec.student_id.name)}
                        ''' sending push notification for each sanction'''
                        notif.sendPushNotification(data=data ,content=content, tokens=[token.token])
        self.write({'state':'accepted'})
        return True

    @api.multi
    def set_rejected(self):
        self.write({'state':'rejected'})
        return True

    @api.multi
    def set_canceled(self):
        self.write({'state':'canceled'})
        return True

    # @api.multi
    # def set_archived(self):
    #     self.write({'state':'archived'})
    #     return True

    @api.multi
    def set_as_paid(self):
        self.write({'state':'paid'})
        return True

    @api.model
    def create(self, vals):
        res = super(Reservation,self).create(vals)
        code = self.env['ir.sequence'].next_by_code('school.reservation')
        end_date = ''
        if res.reservation_type_id:
            if res.reservation_type_id.periode == 'day':
                end_date = res.start_date
            elif res.reservation_type_id.periode == 'week':
                # neeed to be changes to 
                end_date = res.start_date
            elif res.reservation_type_id.periode == 'month':
                # neeed to be changes to 
                end_date = res.start_date
            elif res.reservation_type_id.periode == 'other':
                # neeed to be changes to 
                end_date = res.start_date
            print "### date ",end_date
            print "### date type ",type(end_date)

        res.write({'name':code, 'end_date':end_date})
        return res


    @api.multi
    def write(self, vals):
        for rec in self:
            # we are not updating the state of the reservation 
            # we are updating the reservation attribute while it's state is canceled 
            if not vals.get('state',False) and rec.state == 'canceled':
                raise ValidationError('Vous ne pouvez pas modifier une réservation qui a été annulée')
        return super(Reservation, self).write(vals);




    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state == 'paid':
                raise ValidationError("Vous ne pouvez pas supprimer une réservation dont le statut est Payé")
        return super(Reservation, self).unlink()



class ReservationType(models.Model):
    _name = 'school.reservation.type'
    _description = 'Reservation Type'


    name                        = fields.Char(string='Nom durée', required=True)
    price                       = fields.Float(string='Prix', default=0.0, required=True)
    periode                     = fields.Selection([
                                                    ('day','Jour'),
                                                    ('week','Semaine'),
                                                    ('month','Mois'),
                                                    ('other','Autre'),
                                                    ])

    start_date                  = fields.Date(string='Date début')
    end_date                    = fields.Date(string='Date fin')