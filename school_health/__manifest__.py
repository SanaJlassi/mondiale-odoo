{
    'name': 'School Health',
    'version': '2.0',
    'category': 'School Management',
    'author': 'Odesco Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        'security/ir.model.access.csv',
        'views/school_medical_consultation.xml',
        'views/menuitem.xml',

    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}