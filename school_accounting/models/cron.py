# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import date, datetime


class InvoicePlanner(models.Model):
    _name = 'invoice.planner'
    _description = 'Invoice Planner'


    name                    = fields.Char(string="Nom", required=True)
    active                  = fields.Boolean(help="If the active field is set to False, it will allow you to hide the subscription without removing it.", default=True)
    user_id                 = fields.Many2one('res.users', string='Utilisateur', required=True, default=lambda self: self.env.user)
    #partner_id = fields.Many2one('res.partner', string='Partner')
    notes = fields.Text(string='Internal Notes')
    user_id = fields.Many2one('res.users', string='User', required=True, default=lambda self: self.env.user)
    interval_number = fields.Integer(string='Fréquence de facturation', default=1)
    interval_type = fields.Selection([
                                        ('minutes', 'Minutes - for test'),
                                        ('hours', 'Heurs - for test'),
                                        ('days', 'Jour'), 
                                        ('weeks', 'Semaine'), 
                                        ('months', 'Mois'),
                                        ], string="Unité d'intervalle", default='months')
    exec_init = fields.Integer(string='Nombre de répétition')
    date_init = fields.Datetime(string='Date début', default=fields.Datetime.now())
    state = fields.Selection([('draft', 'Brouillon'), ('running', 'En cours'), ('done', 'Terminé')], string='Statut', copy=False, default='draft')
    #doc_source = fields.Reference(selection=_get_document_types, string='Source Document', required=True, help="User can choose the source document on which he wants to create documents")
    doc_lines = fields.One2many('invoice.planner.history', 'planner_id', string='Documents créés', readonly=True)
    cron_id = fields.Many2one('ir.cron', string='Cron Job', help="Scheduler which runs on subscription", states={'running': [('readonly', True)], 'done': [('readonly', True)]})
    note = fields.Text(string='Notes', help="Description de planificateur")



    @api.multi
    def set_process(self):
        for planner in self:
            cron_data = {
                'name': planner.name,
                'interval_number': planner.interval_number,
                'interval_type': planner.interval_type,
                'numbercall': planner.exec_init,
                'nextcall': planner.date_init,
                'model': self._name,
                'args': repr([[planner.id]]),
                'function': '_create_invoice',
                'priority': 6,
                'user_id': planner.user_id.id
            }
            cron = self.env['ir.cron'].sudo().create(cron_data)
            planner.write({'cron_id': cron.id, 'state': 'running'})

    @api.model
    def _create_invoice(self, ids):
        self.browse(ids).create_invoice()


    @api.multi
    def create_invoice(self):
        for record in self:
            print "##### MSG FROM CRON",record.interval_number
            students = self.env['school.student'].search([('state','in',['pre-registred','registred'])])
            invoices = []
            for student in students:
                print "################################################### START ###################################################"
                print "STUDNET ", student.name
                product_ids = []
                for rec in self.env['school.fees'].search([('grade_id','=',student.grade_id.id)]):
                    print "### SCHOOL FEES ",rec.name
                    invoice_data = {
                        'type':'out_invoice',
                        'state':'draft',
                        'partner_id':student.partner_id.id,
                        'date_invoice':fields.Date.today(),
                        'user_id':self.env.user.id,
                        # 'date_invoice': '2018-03-01',
                        #'invoice_line_ids':product_ids
                    }
                    invoice_id = self.env['account.invoice'].create(invoice_data)

                    fpos = invoice_id.fiscal_position_id
                    company = invoice_id.company_id
                    currency = invoice_id.currency_id
                    type = invoice_id.type

                    #account_id = self.env['account.invoice.line'].get_invoice_line_account(type, product, fpos, company)

                    for product in rec.obligatory_fees_ids:
                        product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
                        print "### PRODUCT ",product.name

                        qte = 1
                        #payment_school_id = self.env['account.payment.line'].search([('student_id','=',student.id),('product_id.name','ilike','Scolarité'),('is_invoiced','=',True)])
                        if product_product.qte_depends_on_periode:
                            print "------------------- INTERVAL ---------------- ",record.interval_number
                            qte *= record.interval_number
                            print "------------------- QTE ---------------- ",qte  

                        account_id = self.env['account.invoice.line'].get_invoice_line_account(type, product_product, fpos, company)
                        product_ids.append((0,0, {'name':product_product.name,'product_id':product_product.id, 'price_unit':product_product.price,'account_id':account_id.id,'quantity':qte}))

                        # for line in payment_school_id:
                        #     if line.account_payment_id.payment_date >=  '2017-11-01' and line.account_payment_id.payment_date <=  '2017-12-31' :
                        #         qte = 2
                        #         line.is_invoiced = True
                        # print '################# qte payment_school_id', qte
                        # account_id = self.env['account.invoice.line'].get_invoice_line_account(type, product_product, fpos, company)
                        # if qte != 0:
                        #     product_ids.append((0,0, {'name':product_product.name,'product_id':product_product.id, 'price_unit':product_product.price,'account_id':account_id.id,'quantity':qte}))


                    for product in rec.optional_fees_ids:
                        product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
                        print "### PRODUCT ",product.name

                        qte = 0
                        month_now = int(datetime.now().strftime("%m"))
                        payment_canteen_id = self.env['account.payment.line'].search([('student_id','=',student.id),('product_id.name','ilike','Cantine')])
                        for line in payment_canteen_id:
                            #if line.account_payment_id.payment_date >=  '2017-09-15' and line.account_payment_id.payment_date <=  '2017-10-31' :
                            if line.month == month_now-1 :
                                print '################# month_now-1', line.month
                                qte += 1 
                                line.is_invoiced = True
                                print '################# qte month_now-1', qte

                            
                            #if line.account_payment_id.payment_date >=  '2017-11-01' and line.account_payment_id.payment_date <=  '2017-12-31' and line.month == 'Novembre'  line.month == 'Décembre' :
                            if line.month == month_now-2 :
                                print '################# month_now-2', line.month
                                qte += 1 
                                line.is_invoiced = True
                                print '################# qte month_now-2 ', qte
                        
                        print '################# qte payment_canteen_id', qte
                        account_id = self.env['account.invoice.line'].get_invoice_line_account(type, product_product, fpos, company)
                        if qte != 0:
                            product_ids.append((0,0, {'name':product_product.name,'product_id':product_product.id, 'price_unit': product_product.price,'account_id':account_id.id,'quantity':qte}))



                    
                    invoice_id.write({'invoice_line_ids':product_ids})
                    for line in invoice_id.invoice_line_ids:
                        print "### LINE ",line.name
                        line._onchange_product_id()
                    invoice_id.compute_taxes()
                    print "#################################################### END ##################################################"





    @api.multi
    def unlink(self):
        if any(self.filtered(lambda s: s.state == "running")):
            raise UserError(_('You cannot delete an active planner!'))
        return super(InvoicePlanner, self).unlink()

    @api.multi
    def set_done(self):
        self.mapped('cron_id').write({'active': False})
        self.write({'state': 'done'})

    @api.multi
    def set_draft(self):
        self.write({'state': 'draft'})


    class SubscriptionHistory(models.Model):
        _name = "invoice.planner.history"
        _description = "Planner history"
        _rec_name = 'date'

        date = fields.Datetime()
        planner_id = fields.Many2one('invoice.planner', string='Planificateur de facturation', ondelete='cascade')
        # document_id = fields.Reference(selection=_get_document_types, string='Source Document', required=True)
