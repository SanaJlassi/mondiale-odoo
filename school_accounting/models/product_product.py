# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import odoo.addons.decimal_precision as dp


''' 
    @To-Do 
    inherit from product.product because account.invoice.line is using the product.product class
    and the product.template may generate conflicts because many product.product can be related 
    to one product.template

    update :meth _onchange_partner_id() access the product.product directly without product_templ_id
    because right now the search can return a recordset with multi values
 '''
class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_school_fees      		= fields.Boolean('Is School Fees')
    obligatory          		= fields.Boolean('Obligatory Fees',default=False)
    academic_year_id    		= fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), required=True)
    qte_depends_on_periode  	= fields.Boolean(string="Qté dépend période", default=False)
    product_type                = fields.Selection([('primary', 'Principale'), ('auxiliary', 'Auxiliaire')],default='primary', required=True, string='Nature des frais')



    price_ttc                   = fields.Float(string="Prix TTC", default=1.0, digits=dp.get_precision('Product Price'), compute='_compute_price')
    list_price = fields.Float(
        'Sale Price', default=1.0,
        digits=dp.get_precision('Product Price'),
        help="Base price to compute the customer price. Sometimes called the catalog price.")


    @api.depends('list_price')
    @api.multi
    def _compute_price(self):
        for rec in self:
            amount_tax = sum(line.amount for line in rec.taxes_id)
            print "### amount of tax ", amount_tax
            print "### amount of tax ", amount_tax/100
            rec.price_ttc =  rec.list_price * (1+(amount_tax/100))
