# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)


class StateInvoiceWizard(models.TransientModel):
    _name = 'state.invoice.report'


    #from_date   =   fields.Date('Date début', default=fields.Date.today(), required=True)
    #to_date     =   fields.Date('Date fin', default=fields.Datetime.now(), required=True)
    from_date   =   fields.Date('Date début', default=datetime.today(), required=True)
    to_date     =   fields.Date('Date fin', default=datetime.today(), required=True)
    #invoices_id =  fields.Many2one('account.invoice', string='Invoice', required=True)



    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['from_date','to_date'])[0]
        #print "######### DATA ",data
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['from_date','to_date'])[0])
        #print "####### UPDATED DATA ",data
        return self.env['report'].get_action(self, 'school_accounting.report_invoice_state', data=data)



class StateInvoiceReport(models.AbstractModel):
    _name = 'report.school_accounting.report_invoice_state'


    def get_ttc_sum(self,invoices):
        #print "----------||||||||||||||||||||||||| invoices ",invoices
        ttc = 0.0
        for inv in invoices:
            ttc+= (inv.amount_untaxed+inv.amount_tax)
        #print "#######----------------------------------- TOTAL", ttc
        return ttc

    def get_total_ttc(self,invoices):
        tttc = 0.0
        for inv in invoices:
            tttc+= inv.amount_total
        return tttc

    def get_total_timbre(self,invoices):
        timbre = 0.0
        for inv in invoices:
            timbre+= inv.tax_stamp_value
        return timbre

    def get_total_tax(self,invoices):
        tax = 0.0
        for inv in invoices:
            #print "######--------------------------------------------------------- tax ",inv.amount_tax
            tax+= inv.amount_tax
        #print "######--------------------------------------------------------- total tax ",tax
        return tax



    
    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        #print "################# docs ",docs
        invoice_records = []
        if not docs.from_date or not docs.to_date:
            raise UserError("Please enter duration")
        invoices = self.env['account.invoice'].search(['&',('date_invoice','>=',docs.from_date),('date_invoice','<=',docs.to_date)])
        #print "####### ",invoices

        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'invoices': invoices,
            'get_ttc_sum':self.get_ttc_sum,
            'get_total_ttc':self.get_total_ttc,
            'get_total_timbre':self.get_total_timbre,
            'get_total_tax':self.get_total_tax,
        }
        #print "################# docargs ",docargs
        return self.env['report'].render('school_accounting.report_invoice_state', docargs)