{
    'name': 'School Configuration',
    'version': '2.0',
    'category': 'School Management',
    'author': 'Odesco Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        'views/school_config.xml',
        'views/school_config_settings.xml',
        'views/menuitem.xml',

        

    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}