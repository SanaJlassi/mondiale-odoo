# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime



class SchoolAppointment(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/appointments/categories', type='http', auth="user", methods=['GET'],  csrf=False)
    def appointment(self, **kwargs):
        response = {'success':False, 'data':None}
        categories = request.env['calendar.event.type'].search([])
        cat_list = []
        for rec in categories:
            cat_list.append({'id':rec.id, 'name':rec.name, 'type':rec.type})

        response['success'] = True
        response['data'] = cat_list
        return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)




    @http.route('/api/appointment', type='http', auth="user", methods=['GET','POST'],  csrf=False)
    def create_meeting(self, **kwargs):
        response = {'success':False, 'data':None}
        if request.httprequest.method == 'POST':

            if not request.params.get('appointment',False):
                response['success'] = False
                response['error'] = {'code':404, 'message':'Missing object appointment'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            json_object = None
            try:
                json_object = json.loads(request.params.get('appointment',False))
                print "JSON-OBJECT ",json_object
            except ValueError, e:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Unable to deserialize appointment object'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            # if not json_object['teacher_ids']:
            #     response['success'] = False
            #     response['error'] = {'code':404, 'message':'Missing id of the teacher'} 
            #     return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)
            if not json_object['category_id']:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Missing id of the category'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            if not json_object['description']:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Description of the meeting is missing'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)       

            if not json_object['name']:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Titel of the meeting is missing'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

                # {
                #     'teacher_ids':[1,2,3],
                #     'category_id':id,
                #     'description':'string,'
                #     'name':'string'
                # }
            partner_ids = []
            if 'teacher_ids' in json_object:
                partner_ids = json_object['teacher_ids']


            hr_list = request.env['hr.employee'].browse(partner_ids)
            partner_ids = []
            for hr in hr_list:
                print "#### partner_id name",hr.partner_id.name
                partner_ids.append(hr.partner_id.id)

            print "###### Content of partner_ids ",partner_ids



            categ_ids = [json_object['category_id']]
            type = request.env['calendar.event.type'].browse(categ_ids)
            for el in type:
                print "##### TYPE ",el.name
                print "##### TYPE ",el.type
            print "### partner_ids ",partner_ids
            print "### categ_ids ",categ_ids


            appointment_object = {
                'privacy':'public',
                'location':'location of the meeting',
                'name':json_object['name'],
                'description':json_object['description'],
                'partner_ids':[[6,False,partner_ids]] ,
                'categ_ids': [[6,False,categ_ids]] ,
                'user_id':request.env.user.id,
                'start':fields.Datetime.now(),
                'start_datetime':fields.Datetime.now(),
                'stop':fields.Datetime.now(),
                'stop_datetime':fields.Datetime.now(),
            }

            print "### appointment_OBJECT ",appointment_object

            res = request.env['calendar.event'].create(appointment_object)
            print "#### RES ",res
            response['success'] = True
            response['data'] = None
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)
        else:
            response = {'success':False, 'data':None}
            meetings  = request.env['calendar.event'].search([('user_id','=',request.env.user.id)])
            meeting_list = []
            for meeting in meetings:
                partner_ids = []
                for el in meeting.partner_ids:
                    partner = {
                        'id':el.id,
                        'name':el.name,
                        'last_name':el.last_name
                    }
                    partner_ids.append(partner)
                categories = []
                for categ in meeting.categ_ids:
                    categories.append({'id':categ.id,'name':categ.name,'type':categ.type})
                json_object = {
                    'id':meeting.id,
                    'name':meeting.name,
                    'partner_ids':partner_ids,
                    'start_datetime':meeting.start_date,
                    'duration':meeting.end_date,
                    'type':categories,
                    'state':meeting.state,
                }
                meeting_list.append(json_object)
            response['success'] = True
            response['data'] = meeting_list
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

        



   

        

