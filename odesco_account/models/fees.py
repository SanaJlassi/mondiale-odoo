# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError



class FeesPeriode(models.Model):
	_name  = 'o.fees'


	name 				= fields.Char(string='Titre de frais', required=True)
	order 				= fields.Integer(string="Order d'affichage")
	active 				= fields.Boolean(string='Active', default=True)
	invoiceable			= fields.Boolean(string='Facturable', default=False)



class FeesPeriode(models.Model):
	_name = 'o.fees.periode'


	name 				= fields.Char(string='Titre', required=True)
	start_date			= fields.Date(string='Date début', required=True)
	end_date			= fields.Date(string='Date fin', required=True)
	order 				= fields.Integer(string="Order d'affichage")
	active 				= fields.Boolean(string='Active', default=True)
