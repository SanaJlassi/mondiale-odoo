# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class MessageSent(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/parent/messages', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_messages(self, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),MessageSent.HEADER)

        messages = request.env['message.sent'].search([('partner_id','=',partner_id.id)])
    
        messages_list = []
        for message in messages:
            file_list = []
            for file in message.file_ids:
                _logger.warning('####### FILE NAME %s',file.file_name)
                _logger.warning('####### FILE NAME %s',file.description)
                domain = [
                    ('res_model', '=', file._name),
                    ('res_field', '=', 'file'),
                    ('res_id', '=', file.id),
                ]
                attachment = request.env['ir.attachment'].search(domain)
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+file.file_name
                url = url.replace(" ", "_")
                _logger.warning('#### URL of document %s',url)
                file_list.append({
                        'description':file.description,
                        'file_name':file.file_name,
                        'url':url,
                    })
                _logger.warning('##### FILE url %s',url)

            json_object = {
                'title':message.name,
                'content':message.content,
                'datetime':message.datetime,
                'files': file_list,
            }
            messages_list.append(json_object)


        response['success'] = True
        response['data'] = messages_list
        return http.request.make_response(json.dumps(response),MessageSent.HEADER)
