# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class ParentRelation(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }

        


    @http.route('/api/parent/relation', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_relation(self, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),ParentRelation.HEADER)


        partner_id  = request.env.user.partner_id
        student_ids = partner_id.student_ids

        class_list = []
        for student in student_ids:
            class_list.append(student.class_id)


        relation_id = request.env['school.parent.relation'].search([('class_id','in',[class_id.id for class_id in class_list])])
        relation_list = []
        _logger.warning('####### relation_id %s', relation_id)
        for el in relation_id:
            _logger.warning('##### DESCRIPRIPRION %s',el.description)
            fiel_list = []
            for file in el.file_ids:
                _logger.warning('####### FILE NAME %s',file.file_name)
                _logger.warning('####### FILE NAME %s',file.description)
                domain = [
                    ('res_model', '=', file._name),
                    ('res_field', '=', 'file'),
                    ('res_id', '=', file.id),
                ]
                attachment = request.env['ir.attachment'].search(domain)
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+file.file_name
                url = url.replace(" ", "_")
                _logger.warning('#### URL of document %s',url)
                fiel_list.append({
                        'id':file.id,
                        'description':file.description,
                        'file_name':file.file_name,
                        'url':url,
                    })
                _logger.warning('##### FILE url %s',url)


            json_object = {
                'id':el.id,
                'description':el.description,
                'datetime':el.datetime,
                'class':{
                    'id':el.class_id.id,
                    'name':el.class_id.name,
                    'code':el.class_id.code,
                },
                'files':fiel_list,

            }
            relation_list.append(json_object)



        response['data'] = relation_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),ParentRelation.HEADER)
