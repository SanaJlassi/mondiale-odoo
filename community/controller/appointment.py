# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime  , timedelta



class SchoolAppointment(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/primary/teachers', type='http', auth="user", methods=['GET'],  csrf=False)
    def primary_teachers(self, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

        teacher_ids = partner_id.contact_ids
        primary_teacher_ids = []
        for teacher in teacher_ids:
            print "#### TEACHER ",teacher.name
            if teacher.is_primary:
            
                print "#### TEACHER IS PRIMARY ",teacher.name
                print "#### TEACHER IS LAST_NAME ",teacher.last_name
                primary_teacher_ids.append(teacher)

        subjects = []
        contact_list = []
        for teacher in primary_teacher_ids:
            for subject in teacher.subject_ids:
                subjects.append({'id':subject.id, 'name':subject.name, 'code':subject.code})
            domain = [
                ('res_model', '=', teacher._name),
                ('res_field', '=', 'image'),
                ('res_id', '=', teacher.id),
            ]
            attachment = request.env['ir.attachment'].search(domain)
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'
            url = url.replace(" ", "_")
            json_object = {
                'id':teacher.id,
                'name':teacher.name,
                'last_name':teacher.last_name,
                'image':url,
                'subjects':subjects,
            }
            contact_list.append(json_object)

        response['success'] = True
        response['data'] = contact_list
        return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)


    @http.route('/api/appointment/<int:id>/delete', type='http', auth="user", methods=['POST'],  csrf=False)
    def appointment_delete(self,id, **kwargs):
        response = {'success':False, 'data':None}
        print "##### ID ",request.params.get('id',False)
        appointment = request.env['school.appointment'].search([('id','=',id)])
        if not appointment or appointment.state != 'waiting':
            response['success'] = False
            response['error'] = {'code':404, 'message':'you can only delete an appointment in wating state'} 
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)
        print "### appointment ",appointment.subject
        res = appointment.unlink()

        if not res:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to delete the appointment'} 
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)
        else:
            response['success'] = True
            response['data'] = None
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)




    @http.route('/api/teacher/appointments', type='http', auth="user", methods=['GET'],  csrf=False)
    def teacher_appointment(self, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        teacher_id = request.env['hr.employee'].search([('partner_id','=',partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to identify the teacher'} 
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

        appointment_ids = request.env['school.appointment'].search([('state','=','accepted')])
        appointment_list = []
        for app in appointment_ids:
            if teacher_id.id in app.teacher_ids.ids:
                appointment_list.append(app)
        appointment_obj_list = []
        for app in appointment_list:
            teachers = []
            for teacher in app.teacher_ids:
                teachers.append({
                        'id':teacher.id,
                        'name':teacher.name,
                        'last_name':teacher.last_name,
                        'image':teacher.image,
                    })
            json_object = {
                'id':app.id,
                'objet':app.name,
                'subject':app.subject,
                'type':app.type,
                'note':app.note,
                'state':app.state,
                'student':{
                    'id':app.student_id.id,
                    'name':app.student_id.name,
                    'last_name':app.student_id.last_name,
                },
                'start_date':app.start_date,
                'end_date':app.end_date,
                'teacher_ids':teachers,
            }
            appointment_obj_list.append(json_object)
        response['success'] = True
        response['data'] = appointment_obj_list
        return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)




    @http.route('/api/appointments', type='http', auth="user", methods=['GET','POST'],  csrf=False)
    def appointment(self, **kwargs):
        response = {'success':False, 'data':None}
        if request.httprequest.method == 'POST':
            partner_id = request.env.user.partner_id
            appointment_ids = request.env['school.appointment'].search(['&',('parent_id','=',partner_id.id), ('state','=','waiting')])
            student_ids = partner_id.student_ids

            if len(appointment_ids) >= len(student_ids):
                response['success'] = False
                response['error'] = {'code':404, 'message':'You have reached the max number of waiting appointments'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            if not request.params.get('appointment',False):
                response['success'] = False
                response['error'] = {'code':404, 'message':'Missing object appointment'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            json_object = None
            try:
                json_object = json.loads(request.params.get('appointment',False))
                print "JSON-OBJECT ",json_object
            except ValueError, e:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Unable to deserialize appointment object'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            if not json_object['student_id']:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Student id is missing'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            if not json_object['type']:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Type of the meeting is missing'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            if 'pedagogic' == json_object['type'] and  not json_object['teacher_ids']:
                response['success'] = False
                response['error'] = {'code':404, 'message':'teacher_ids of the meeting is missing'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)       

            if not json_object['subject']:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Subject of the meeting is missing'} 
                return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)

            teacher_ids = request.env['hr.employee'].browse(json_object['teacher_ids'])

            appointment_object = {
                'name':json_object['name'] if 'name' in json_object else '',
                'subject':json_object['subject'],
                'type':json_object['type'],
                'teacher_ids':[[6,False,teacher_ids.ids]],
                'note':json_object['note'] if 'note' in json_object else '',
                'state':'waiting',
                'parent_id':request.env.user.partner_id,
                'student_id':json_object['student_id'],
                'start_date':fields.Datetime.now(),
                'end_date':fields.Datetime.now(),
            }
            print "### appointment_OBJECT ",appointment_object

            res = request.env['school.appointment'].create(appointment_object)
            print "#### RES ",res
            response['success'] = True
            response['data'] = None
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)
        else:
            partner_id = request.env.user.partner_id
            appointment_ids = request.env['school.appointment'].search([('parent_id','=',partner_id.id)])
            
            appointment_list = []
            for app in appointment_ids:
                teachers = []
                for teacher in app.teacher_ids:
                    teachers.append({
                            'id':teacher.id,
                            'name':teacher.name,
                            'last_name':teacher.last_name,
                            'image':teacher.image,
                        })
                json_object = {
                    'id':app.id,
                    'objet':app.name,
                    'subject':app.subject,
                    'type':app.type,
                    'note':app.note,
                    'student':{
                        'id':app.student_id.id,
                        'name':app.student_id.name,
                        'last_name':app.student_id.last_name,
                    },
                    'state':app.state,
                    'start_date':app.start_date,
                    'end_date':app.end_date,
                    'teacher_ids':teachers,
                }
                appointment_list.append(json_object)
            response['success'] = True
            response['data'] = appointment_list
            return http.request.make_response(json.dumps(response),SchoolAppointment.HEADER)


   