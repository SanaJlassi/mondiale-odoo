# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime



class ContactAs(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/messages', type='http', auth="user", methods=['GET','POST'],  csrf=False)
    def get_messages(self, **kwargs):
        response = {'success':False, 'data':None}
        if request.httprequest.method == 'GET':
            data = []
            if not request.env.user.has_group('genext_school.group_school_parent'):
                response['success'] = False
                response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
                return http.request.make_response(json.dumps(response),ContactAs.HEADER)
            else:         
                #res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
                message_ids = request.env['contact.as'].search([('user_id','=',request.env.user.id)])
                msg_list = []
                for msg in message_ids:
                    json_object = {
                        'id':msg.id,
                        'title':msg.name,
                        'content':msg.content,
                        'datetime':msg.datetime,
                        'user':{
                            'id':msg.user_id.id,
                            'name':msg.user_id.name,
                            'last_name':msg.user_id.last_name
                        }
                    }
                    msg_list.append(json_object)
            response['success'] = True
            response['data'] = msg_list
            return http.request.make_response(json.dumps(response),ContactAs.HEADER)
        else:
            json_object = None
            try:
                json_object = json.loads(request.params.get('message',False))
                print "MESSAGE JSON-OBJECT ",json_object
            except ValueError, e:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Unable to deserialize message object'} 
                return http.request.make_response(json.dumps(response),ContactAs.HEADER)

            message_object = {
                'name':json_object['title'],
                'content':json_object['content'],
                'partner_id':request.env.user.partner_id.id
            }

            res = request.env['contact.as'].create(message_object)

            print "### messsage creation res ",res
            json_object = {
                'id':res.id,
                'title':res.name,
                'content':res.content,
                'datetime':res.datetime,
                'user':{
                    'id':res.user_id.id,
                    'name':res.user_id.name,
                    'last_name':res.user_id.last_name
                }
            }
            response['success'] = True
            response['data'] = None
            return http.request.make_response(json.dumps(response),ContactAs.HEADER)
