# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class SMSSenderWizard(models.TransientModel):
	_name  			=  'sms.sender.wizard'
	_description 	=	'A helper to send sms to multiple users'


	name 			= fields.Char(string="")
	user_type 		= fields.Selection([('parent','Parent'),('teacher','Enseignant'), ('student','Élève')], default='parent')
	parent_ids 		= fields.Many2many('res.partner')


	sms_id 			= fields.Many2one('school.sms')
