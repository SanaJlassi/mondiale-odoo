{
    'name': 'Communauté',
    'version': '1.0',
    'category': 'Scolarité',
    'author': 'Ben Selma Sghaier',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/community_view.xml',
    ],
    #'qweb': ['static/src/xml/*.xml'],

    'installable': True,
    'auto_install': False,
    'application': True,
}